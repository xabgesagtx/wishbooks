import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SharedModule} from './shared/shared.module';
import {AuthModule} from './auth/auth.module';
import {MainModule} from './main/main.module';
import {HttpClientModule} from '@angular/common/http';
import {SearchModule} from './search/search.module';
import {BooksModule} from './books/books.module';
import {ListsModule} from './lists/lists.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReviewModule} from './review/review.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    AuthModule,
    SearchModule,
    MainModule,
    BooksModule,
    ListsModule,
    ReviewModule,
    BrowserAnimationsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
