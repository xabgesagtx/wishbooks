import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ReviewListComponent} from './review-list.component';
import {BookService} from '../../books/book.service';
import {of} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

describe('ReviewListComponent', () => {
  let component: ReviewListComponent;
  let fixture: ComponentFixture<ReviewListComponent>;
  const bookServiceSpy = jasmine.createSpyObj('BookService', {
    booksReadInYear: of([])
  });
  const years = [2024];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReviewListComponent],
      providers: [
        {provide: BookService, useValue: bookServiceSpy},
        {
          provide: ActivatedRoute, useValue: {
            data: of({years})
          }
        },
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ReviewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
