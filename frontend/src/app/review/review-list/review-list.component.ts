import {Component, OnInit} from '@angular/core';
import {BookService} from '../../books/book.service';
import {ActivatedRoute} from '@angular/router';
import {Book} from '../../books/model/book';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.scss']
})
export class ReviewListComponent implements OnInit {

  books: Book[] = [];
  years: number[] = [];
  yearFilter: number;

  constructor(private bookService: BookService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.data.subscribe((data: { years: number[] }) => {
      this.years = data.years;
      if (this.years.length > 0) {
        this.yearFilter = Math.max(...this.years);
        this.filterChanged();
      }
    });
  }

  filterChanged() {
    if (this.yearFilter) {
      this.bookService.booksReadInYear(this.yearFilter).subscribe(books => this.books = books);
    }
  }

}
