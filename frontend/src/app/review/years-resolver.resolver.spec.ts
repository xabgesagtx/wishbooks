import {TestBed} from '@angular/core/testing';
import {ResolveFn} from '@angular/router';

import {yearsResolverResolver} from './years-resolver.resolver';

describe('yearsResolverResolver', () => {
  const executeResolver: ResolveFn<number[]> = (...resolverParameters) =>
    TestBed.runInInjectionContext(() => yearsResolverResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });
});
