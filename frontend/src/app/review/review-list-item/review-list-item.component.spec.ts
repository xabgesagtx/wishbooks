import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ReviewListItemComponent} from './review-list-item.component';
import {AuthorsPipe} from '../../shared/authors.pipe';
import {BookStatus} from '../../books/model/book-status';

describe('ReviewListItemComponent', () => {
  let component: ReviewListItemComponent;
  let fixture: ComponentFixture<ReviewListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReviewListItemComponent, AuthorsPipe]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ReviewListItemComponent);
    component = fixture.componentInstance;
    component.book = {
      authors: [],
      createdAt: '1970-01-01T00:00:00.000',
      externalId: 'externalId',
      id: 'id',
      language: 'en',
      modifiedAt: '1970-01-01T00:00:00.000',
      status: BookStatus.WANTED,
      title: 'title',
      version: 0
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
