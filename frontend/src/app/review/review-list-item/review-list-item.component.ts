import {Component, Input} from '@angular/core';
import {Book} from '../../books/model/book';
import {DateTime} from 'luxon';

@Component({
  selector: 'app-review-list-item',
  templateUrl: './review-list-item.component.html',
  styleUrls: ['./review-list-item.component.scss']
})
export class ReviewListItemComponent {

  @Input()
  book: Book;

  get readAt() {
    return DateTime.fromISO(this.book.readAt).toFormat('dd.MM.yyyy');
  }
}
