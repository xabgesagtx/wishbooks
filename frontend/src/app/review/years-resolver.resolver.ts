import {ResolveFn, Router} from '@angular/router';
import {inject} from '@angular/core';
import {BookService} from '../books/book.service';
import {catchError} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';

export const yearsResolverResolver: ResolveFn<number[]> = (route, state) => {
  const router = inject(Router);
  return inject(BookService).yearForReadBooks().pipe(catchError((err: HttpErrorResponse) => {
    if (err.status === 404) {
      router.navigate(['/not-found']);
    } else {
      router.navigate(['/some-error']);
    }
    return EMPTY as Observable<number[]>;
  }));
};
