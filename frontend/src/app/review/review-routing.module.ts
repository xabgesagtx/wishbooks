import {RouterModule, Routes} from '@angular/router';
import {LoggedInGuard} from '../auth/logged-in.guard';
import {NgModule} from '@angular/core';
import {ReviewListComponent} from './review-list/review-list.component';
import {yearsResolverResolver} from './years-resolver.resolver';

export const routes: Routes = [
  {
    path: 'review',
    component: ReviewListComponent,
    canActivate: [LoggedInGuard],
    resolve: {years: yearsResolverResolver}
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {})
  ],
  exports: [RouterModule]
})
export class ReviewRoutingModule {
}
