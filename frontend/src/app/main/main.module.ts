import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HomeComponent} from './home/home.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NotFoundComponent} from './not-found/not-found.component';
import {NavigationComponent} from './navigation/navigation.component';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [
    HomeComponent,
    NavigationComponent,
    NotFoundComponent
  ],
  exports: [
    HomeComponent,
    NavigationComponent,
    NotFoundComponent
  ],
  imports: [
    NgbModule,
    CommonModule,
    FontAwesomeModule,
    RouterModule
  ],
  providers: [
    HomeComponent,
    NotFoundComponent,
    NavigationComponent
  ]
})
export class MainModule { }
