import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {faBookOpen, faSignInAlt, faSignOutAlt} from '@fortawesome/free-solid-svg-icons';
import {NavigationStart, Router} from '@angular/router';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  isNavbarCollapsed = true;

  faBookOpen = faBookOpen;
  faSignOutAlt = faSignOutAlt;
  faSignInAlt = faSignInAlt;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.router.events.pipe(
      filter(event => event instanceof NavigationStart)
    ).subscribe(() => { this.isNavbarCollapsed = true; });
  }

  login() {
    this.authService.login();
  }

  logout() {
    this.authService.logout();
  }

  get username() {
    return this.authService.username;
  }

  get loggedIn() {
    return this.authService.loggedIn;
  }

}
