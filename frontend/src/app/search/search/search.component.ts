import {Component, OnDestroy, OnInit} from '@angular/core';
import {SearchService} from '../search.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from '../../shared/message.service';
import {BookSearchItem} from '../model/book-search-item';
import {Subscription} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  searchStarted = false;
  reachedEnd = false;
  loading = false;
  query: string;
  language = 'all';
  queryForLastSearch: string;
  languageForLastSearch: string;
  subscription: Subscription;
  books: BookSearchItem[] = [];
  page = 0;

  constructor(private searchService: SearchService,
              private router: Router,
              private route: ActivatedRoute,
              private messageService: MessageService) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.query = params.query;
      this.language = params.language ? params.language : 'all';
      if (this.query) {
        this.initSearch();
      } else {
        this.searchStarted = false;
        this.reachedEnd = false;
        this.queryForLastSearch = undefined;
        this.languageForLastSearch = 'all';
        this.books = [];
        this.page = 0;
        if (this.subscription) {
          this.subscription.unsubscribe();
        }
      }
    });
  }

  private initSearch() {
    this.queryForLastSearch = this.query;
    this.languageForLastSearch = this.language;
    this.books = [];
    this.reachedEnd = false;
    this.searchStarted = true;
    this.page = 0;
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.loading = false;
    this.search();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onSubmit() {
    if (this.query === this.queryForLastSearch && this.language === this.languageForLastSearch) {
      this.initSearch();
    } else {
      this.router.navigate(['/search'], {
        queryParams: {
          query: this.query,
          language: this.language
        }
      });
    }
  }

  search() {
    if (!this.reachedEnd && !this.loading) {
      this.loading = true;
      const page = this.page;
      this.subscription = this.searchService.search(this.queryForLastSearch, this.languageForLastSearch, page)
        .subscribe(
          searchResponse => {
            this.reachedEnd = searchResponse.page + 1 >= searchResponse.numberOfPages;
            this.loading = false;
            this.page = searchResponse.page + 1;
            searchResponse.books.forEach(entry => this.books.push(entry));
            delete this.subscription;
          },
          (error: HttpErrorResponse) => {
            this.messageService.showError(`Search failed: ${error.status}: ${error.message}`);
            this.loading = false;
            delete this.subscription;
          });
    }
  }


}
