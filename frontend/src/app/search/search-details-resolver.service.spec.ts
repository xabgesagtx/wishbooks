import {TestBed} from '@angular/core/testing';

import {SearchDetailsResolverService} from './search-details-resolver.service';
import {SearchService} from './search.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('SearchDetailsResolverService', () => {
  const searchServiceSpy = jasmine.createSpyObj('SearchService', ['details']);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule
    ],
    providers: [
      SearchDetailsResolverService,
      {provide: SearchService, useValue: searchServiceSpy}
    ]
  }));

  it('should be created', () => {
    const service: SearchDetailsResolverService = TestBed.inject(SearchDetailsResolverService);
    expect(service).toBeTruthy();
  });
});
