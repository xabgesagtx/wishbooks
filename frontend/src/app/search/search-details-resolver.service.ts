import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {BookDetails} from './model/book-details';
import {EMPTY, Observable} from 'rxjs';
import {SearchService} from './search.service';
import {catchError, map, take} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class SearchDetailsResolverService implements Resolve<BookDetails> {

  constructor(private searchService: SearchService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BookDetails> | Promise<BookDetails> | BookDetails {
    const id = route.paramMap.get('id');
    return this.searchService.details(id).pipe(
      take(1),
      map(response => {
        if (response) {
          return response;
        } else {
          this.router.navigate(['/not-found']);
          return null;
        }
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status === 404) {
          this.router.navigate(['/not-found']);
        } else {
          this.router.navigate(['/some-error']);
        }
        return EMPTY as Observable<BookDetails>;
      })
    );
  }
}
