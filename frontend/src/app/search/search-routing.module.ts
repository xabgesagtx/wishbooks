import {SearchComponent} from './search/search.component';
import {LoggedInGuard} from '../auth/logged-in.guard';
import {SearchDetailsComponent} from './search-details/search-details.component';
import {SearchDetailsResolverService} from './search-details-resolver.service';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

const routes: Routes = [
  { path: 'search', component: SearchComponent, canActivate: [LoggedInGuard] },
  { path: 'search/:id', component: SearchDetailsComponent, canActivate: [LoggedInGuard], resolve: {
      details: SearchDetailsResolverService
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {})
  ],
  exports: [RouterModule]
})
export class SearchRoutingModule {

}
