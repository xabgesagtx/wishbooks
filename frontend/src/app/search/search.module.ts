import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SearchComponent} from './search/search.component';
import {SearchListItemComponent} from './search-list-item/search-list-item.component';
import {SearchService} from './search.service';
import {SearchDetailsResolverService} from './search-details-resolver.service';
import {SharedModule} from '../shared/shared.module';
import {SearchDetailsComponent} from './search-details/search-details.component';
import {RouterModule} from '@angular/router';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';


@NgModule({
  declarations: [
    SearchComponent,
    SearchListItemComponent,
    SearchDetailsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule,
    FontAwesomeModule,
    InfiniteScrollModule
  ],
  providers: [
    SearchService,
    SearchDetailsResolverService
  ]
})
export class SearchModule { }
