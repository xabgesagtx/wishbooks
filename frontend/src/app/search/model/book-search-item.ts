export class BookSearchItem {
  id: string;
  internalId?: string;
  title: string;
  authors: string[];
  publicationDate?: string;
  thumbnailUrl?: string;
}
