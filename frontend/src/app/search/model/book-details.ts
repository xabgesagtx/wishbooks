export class BookDetails {
  id: string;
  internalId?: string;
  title: string;
  authors: string[];
  publicationDate?: string;
  description?: string;
  thumbnailUrl?: string;
  googleUrl: string;
  isbn10?: string;
  isbn13?: string;
  language: string;
  pageCount?: number;
  publisher?: string;
}
