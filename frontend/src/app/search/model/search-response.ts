import {BookSearchItem} from './book-search-item';

export class SearchResponse {
  page: number;
  numberOfPages: number;
  books: BookSearchItem[];
}
