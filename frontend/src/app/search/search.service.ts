import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SearchResponse} from './model/search-response';
import {BookDetails} from './model/book-details';

@Injectable()
export class SearchService {

  constructor(private httpClient: HttpClient) {
  }

  search(query: string, language: string, page: number): Observable<SearchResponse> {
    let params = new HttpParams().append('query', query)
      .append('page', page.toString());
    if (language) {
      params = params.append('language', language);
    }
    return this.httpClient.get<SearchResponse>('/api/search', {params});
  }

  details(id: string): Observable<BookDetails> {
    return this.httpClient.get<BookDetails>(`/api/search/${id}`);
  }
}
