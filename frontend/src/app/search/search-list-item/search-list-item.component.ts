import {Component, Input} from '@angular/core';
import {BookSearchItem} from '../model/book-search-item';
import {faListAlt, faPlusCircle} from '@fortawesome/free-solid-svg-icons';
import {HttpErrorResponse} from '@angular/common/http';
import {MessageService} from '../../shared/message.service';
import {BookService} from '../../books/book.service';

@Component({
  selector: 'app-search-list-item',
  templateUrl: './search-list-item.component.html',
  styleUrls: ['./search-list-item.component.scss']
})
export class SearchListItemComponent {

  @Input()
  item: BookSearchItem;

  @Input()
  last: boolean;
  faListAlt = faListAlt;
  faPlusCircle = faPlusCircle;

  constructor(private bookService: BookService, private messageService: MessageService) {
  }

  get searchItemClass() {
    if (this.last) {
      return 'search-item';
    } else {
      return 'search-item with-border';
    }
  }

  add() {
    this.bookService.add(this.item.id).subscribe(book => {
        this.messageService.showSuccess('Successfully added book');
        this.item.internalId = book.id;
      },
      (error: HttpErrorResponse) => this.messageService.showError(`Could not add book: ${error.status}`));
  }

}
