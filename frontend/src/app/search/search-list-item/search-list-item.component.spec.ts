import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SearchListItemComponent} from './search-list-item.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AuthorsPipe} from '../../shared/authors.pipe';
import {PublicationDatePipe} from '../../shared/publication-date.pipe';
import {BookService} from '../../books/book.service';
import {MessageService} from '../../shared/message.service';

describe('SearchListItemComponent', () => {
  let component: SearchListItemComponent;
  let fixture: ComponentFixture<SearchListItemComponent>;
  const bookServiceSpy = jasmine.createSpyObj('BookService', ['add']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError', 'showSuccess']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        SearchListItemComponent,
        AuthorsPipe,
        PublicationDatePipe
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      providers: [
        {provide: BookService, useValue: bookServiceSpy},
        {provide: MessageService, useValue: messageServiceSpy},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchListItemComponent);
    component = fixture.componentInstance;
    component.item = {
      authors: [],
      id: 'id',
      title: 'title'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
