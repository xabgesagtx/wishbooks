import {Component, OnInit} from '@angular/core';
import {BookDetails} from '../model/book-details';
import {ActivatedRoute} from '@angular/router';
import {BookService} from '../../books/book.service';
import {MessageService} from '../../shared/message.service';
import {HttpErrorResponse} from '@angular/common/http';
import {faExternalLinkAlt, faListAlt, faPlusCircle} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-search-details',
  templateUrl: './search-details.component.html',
  styleUrls: ['./search-details.component.scss']
})
export class SearchDetailsComponent implements OnInit {

  details: BookDetails;
  faExternalLinkAlt = faExternalLinkAlt;
  faPlusCircle = faPlusCircle;
  faListAlt = faListAlt;

  constructor(private route: ActivatedRoute,
              private bookService: BookService,
              private messageService: MessageService) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: { details: BookDetails }) => {
      this.details = data.details;
    });
  }

  add() {
    this.bookService.add(this.details.id).subscribe(book => {
        this.messageService.showSuccess('Successfully added book');
        this.details.internalId = book.id;
      },
      (error: HttpErrorResponse) => this.messageService.showError(`Could not add book: ${error.status}`));
  }

}
