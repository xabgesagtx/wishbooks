import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SearchDetailsComponent} from './search-details.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {BookService} from '../../books/book.service';
import {MessageService} from '../../shared/message.service';
import {AuthorsPipe} from '../../shared/authors.pipe';
import {PublicationDatePipe} from '../../shared/publication-date.pipe';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {BookDetails} from '../model/book-details';
import {LanguagePipe} from '../../shared/language.pipe';

describe('SearchDetailsComponent', () => {
  const details: BookDetails = {
    authors: [],
    googleUrl: 'https://example.com',
    id: 'id',
    language: 'en',
    title: 'title'
  };
  let component: SearchDetailsComponent;
  let fixture: ComponentFixture<SearchDetailsComponent>;
  const bookServiceSpy = jasmine.createSpyObj('BookService', ['add']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError', 'showSuccess']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        SearchDetailsComponent,
        AuthorsPipe,
        LanguagePipe,
        PublicationDatePipe
      ],
      providers: [
        {provide: BookService, useValue: bookServiceSpy},
        {provide: MessageService, useValue: messageServiceSpy},
        {
          provide: ActivatedRoute, useValue: {
            data: of({details})
          }
        },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
