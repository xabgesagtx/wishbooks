import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BookListsComponent} from './book-lists/book-lists.component';
import {LoggedInGuard} from '../auth/logged-in.guard';
import {BookListDetailsComponent} from './book-list-details/book-list-details.component';
import {BookListResolverService} from './book-list-resolver.service';
import {BookListEditComponent} from './book-list-edit/book-list-edit.component';
import {BookListCreateComponent} from './book-list-create/book-list-create.component';
import {PublicBookListResolverService} from './public-book-list-resolver.service';
import {PublicBookListComponent} from './public-book-list/public-book-list.component';


const routes: Routes = [
  { path: 'lists', component: BookListsComponent, canActivate: [LoggedInGuard] },
  { path: 'lists/new', component: BookListCreateComponent, canActivate: [LoggedInGuard] },
  { path: 'lists/public/:id', component: PublicBookListComponent, resolve: { bookList: PublicBookListResolverService } },
  { path: 'lists/:id', component: BookListDetailsComponent, canActivate: [LoggedInGuard], resolve: {bookList: BookListResolverService}},
  { path: 'lists/:id/edit', component: BookListEditComponent, canActivate: [LoggedInGuard], resolve: {bookList: BookListResolverService}}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, {})
  ],
  exports: [
    RouterModule
  ]
})
export class ListsRoutingModule { }
