import {Injectable} from '@angular/core';
import {UpdateBookListRequest} from './model/update-book-list-request';
import {HttpClient, HttpParams} from '@angular/common/http';
import {CreateBookListRequest} from './model/create-book-list-request';
import {PublicBookList} from './model/public-book-list';
import {BookListSummary} from './model/book-list-summary';
import {BookList} from './model/book-list';

@Injectable({
  providedIn: 'root'
})
export class BookListService {

  private readonly basePath = '/api/lists';

  constructor(private httpClient: HttpClient) {
  }

  create(request: CreateBookListRequest) {
    return this.httpClient.post<BookList>(this.basePath, request);
  }

  delete(id: string) {
    return this.httpClient.delete(`${this.basePath}/${id}`);
  }

  update(id: string, request: UpdateBookListRequest) {
    return this.httpClient.put<BookList>(`${this.basePath}/${id}`, request);
  }

  getSummaries() {
    return this.httpClient.get<BookListSummary[]>(this.basePath);
  }

  getPublicBookList(id: string) {
    return this.httpClient.get<PublicBookList>(`public/lists/${id}`);
  }

  getBookList(id: string) {
    return this.httpClient.get<BookList>(`${this.basePath}/${id}`);
  }

  addBook(listId: string, bookId: string) {
    const params = new HttpParams().append('bookId', bookId);
    return this.httpClient.post(`${this.basePath}/${listId}/add`, null, {params});
  }

  removeBook(listId: string, bookId: string) {
    const params = new HttpParams().append('bookId', bookId);
    return this.httpClient.post(`${this.basePath}/${listId}/remove`, null, {params});
  }

}
