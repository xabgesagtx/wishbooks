import {TestBed} from '@angular/core/testing';

import {BookListResolverService} from './book-list-resolver.service';
import {BookListService} from './book-list.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('BoolListResolverService', () => {
  const bookListServiceSpy = jasmine.createSpyObj('BookListService', ['getBookList']);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule
    ],
    providers: [
      {provide: BookListService, useValue: bookListServiceSpy },
      BookListResolverService
    ]
  }));

  it('should be created', () => {
    const service: BookListResolverService = TestBed.inject(BookListResolverService);
    expect(service).toBeTruthy();
  });
});
