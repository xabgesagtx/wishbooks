import {TestBed} from '@angular/core/testing';

import {PublicBookListResolverService} from './public-book-list-resolver.service';
import {RouterTestingModule} from '@angular/router/testing';
import {BookListService} from './book-list.service';

describe('PublicBookListResolverService', () => {
  const bookListServiceSpy = jasmine.createSpyObj('BookListService', ['getBookList']);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule
    ],
    providers: [
      {provide: BookListService, useValue: bookListServiceSpy},
      PublicBookListResolverService
    ]
  }));

  it('should be created', () => {
    const service: PublicBookListResolverService = TestBed.inject(PublicBookListResolverService);
    expect(service).toBeTruthy();
  });
});
