import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BookListsComponent} from './book-lists/book-lists.component';
import {BookListSummaryComponent} from './book-list-summary/book-list-summary.component';
import {ExternalBookListItemComponent} from './external-book-list-item/external-book-list-item.component';
import {BookListEditComponent} from './book-list-edit/book-list-edit.component';
import {BookListCreateComponent} from './book-list-create/book-list-create.component';
import {BookListDetailsComponent} from './book-list-details/book-list-details.component';
import {PublicBookListResolverService} from './public-book-list-resolver.service';
import {BookListService} from './book-list.service';
import {BookListResolverService} from './book-list-resolver.service';
import {BookListsResolverService} from './book-lists-resolver.service';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {InternalBookListItemComponent} from './internal-book-list-item/internal-book-list-item.component';
import {EditableBookListItemComponent} from './editable-book-list-item/editable-book-list-item.component';
import {PublicBookListComponent} from './public-book-list/public-book-list.component';


@NgModule({
  declarations: [
    BookListsComponent,
    BookListSummaryComponent,
    ExternalBookListItemComponent,
    BookListEditComponent,
    BookListCreateComponent,
    BookListDetailsComponent,
    InternalBookListItemComponent,
    EditableBookListItemComponent,
    PublicBookListComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule,
    FontAwesomeModule
  ], providers: [
    PublicBookListResolverService,
    BookListResolverService,
    BookListsResolverService,
    BookListService
  ]
})
export class ListsModule {
}
