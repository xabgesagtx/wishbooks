import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ExternalBookListItemComponent} from './external-book-list-item.component';
import {AuthorsPipe} from '../../shared/authors.pipe';
import {PublicationDatePipe} from '../../shared/publication-date.pipe';
import {BookListItem} from '../model/book-list-item';
import {LanguagePipe} from '../../shared/language.pipe';

describe('ExternalBookListItemComponent', () => {
  let component: ExternalBookListItemComponent;
  let fixture: ComponentFixture<ExternalBookListItemComponent>;
  const book: BookListItem = {
    authors: [], googleUrl: '', id: '', language: '', title: ''
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        AuthorsPipe,
        LanguagePipe,
        PublicationDatePipe,
        ExternalBookListItemComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalBookListItemComponent);
    component = fixture.componentInstance;
    component.book = book;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
