import {Component, Input} from '@angular/core';
import {BookListItem} from '../model/book-list-item';

@Component({
  selector: 'app-external-book-list-item',
  templateUrl: './external-book-list-item.component.html',
  styleUrls: ['./external-book-list-item.component.scss']
})
export class ExternalBookListItemComponent {

  @Input()
  book: BookListItem;

  @Input()
  last: boolean;

  constructor() { }

  get bookClass() {
    if (this.last) {
      return 'book-list-item d-flex';
    } else {
      return 'book-list-item d-flex with-border';
    }
  }

}
