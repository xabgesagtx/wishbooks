import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookListSummaryComponent} from './book-list-summary.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MessageService} from '../../shared/message.service';
import {BookListService} from '../book-list.service';
import {BookListSummary} from '../model/book-list-summary';
import {SharedModule} from '../../shared/shared.module';

describe('BookListSummaryComponent', () => {
  let component: BookListSummaryComponent;
  let fixture: ComponentFixture<BookListSummaryComponent>;
  const bookListServiceSpy = jasmine.createSpyObj('BookListService', ['delete']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError', 'showSuccess']);
  const list: BookListSummary = {
    bookIds: [], createdAt: '', description: '', id: '', modifiedAt: '', name: ''
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookListSummaryComponent
      ],
      imports: [
        SharedModule
      ],
      providers: [
        {provide: MessageService, useValue: messageServiceSpy},
        {provide: BookListService, useValue: bookListServiceSpy},
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListSummaryComponent);
    component = fixture.componentInstance;
    component.list = list;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
