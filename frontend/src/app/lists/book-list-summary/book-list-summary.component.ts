import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BookListSummary} from '../model/book-list-summary';
import {BookListService} from '../book-list.service';
import {MessageService} from '../../shared/message.service';
import {HttpErrorResponse} from '@angular/common/http';
import {faEdit, faTrashAlt} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-book-list-summary',
  templateUrl: './book-list-summary.component.html',
  styleUrls: ['./book-list-summary.component.scss'],
  preserveWhitespaces: true
})
export class BookListSummaryComponent {

  @Input()
  list: BookListSummary;

  @Output()
  listRemoved = new EventEmitter<string>();

  faEdit = faEdit;
  faTrashAlt = faTrashAlt;

  constructor(private bookListService: BookListService, private messageService: MessageService) {
  }

  get createdAt() {
    return new Date(this.list.createdAt);
  }

  get modifiedAt() {
    return new Date(this.list.modifiedAt);
  }

  delete() {
    this.bookListService.delete(this.list.id).subscribe(() => {
      this.listRemoved.emit(this.list.id);
      this.messageService.showSuccess(`Deleted list ${this.list.name}`);
    }, (error: HttpErrorResponse) => this.messageService.showError(`Could not remove list: ${error.status}`));
  }
}
