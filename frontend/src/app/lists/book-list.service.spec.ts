import {TestBed} from '@angular/core/testing';

import {BookListService} from './book-list.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('BookListService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ]
  }));

  it('should be created', () => {
    const service: BookListService = TestBed.inject(BookListService);
    expect(service).toBeTruthy();
  });
});
