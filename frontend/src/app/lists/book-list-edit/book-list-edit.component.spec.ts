import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookListEditComponent} from './book-list-edit.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MessageService} from '../../shared/message.service';
import {BookListService} from '../book-list.service';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {BookList} from '../model/book-list';

describe('BookListEditComponent', () => {
  let component: BookListEditComponent;
  let fixture: ComponentFixture<BookListEditComponent>;
  const bookListServiceSpy = jasmine.createSpyObj('BookListService', ['update']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError', 'showSuccess']);
  const bookList: BookList = {
    books: [],
    createdAt: '',
    description: '',
    id: '',
    modifiedAt: '',
    name: '',
    publicAccess: false,
    version: 0
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookListEditComponent
      ],
      imports: [
        FormsModule,
        RouterTestingModule
      ],
      providers: [
        {provide: MessageService, useValue: messageServiceSpy},
        {provide: BookListService, useValue: bookListServiceSpy},
        {
          provide: ActivatedRoute, useValue: {
            data: of({
              bookList
            })
          }
        }
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
