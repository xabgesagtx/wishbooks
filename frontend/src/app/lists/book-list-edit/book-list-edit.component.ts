import {Component, OnInit} from '@angular/core';
import {BookList} from '../model/book-list';
import {ActivatedRoute, Router} from '@angular/router';
import {BookListService} from '../book-list.service';
import {MessageService} from '../../shared/message.service';
import {NgForm} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {BookListItem} from '../model/book-list-item';

@Component({
  selector: 'app-book-list-edit',
  templateUrl: './book-list-edit.component.html',
  styleUrls: ['./book-list-edit.component.scss']
})
export class BookListEditComponent implements OnInit {

  constructor(private bookListService: BookListService,
              private messageService: MessageService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  bookList: BookList;

  private static arrayMove(array: BookListItem[], fromIndex: number, toIndex: number) {
    const element = array[fromIndex];
    array.splice(fromIndex, 1);
    array.splice(toIndex, 0, element);
  }

  ngOnInit() {
    this.route.data.subscribe((data: { bookList: BookList }) => {
      this.bookList = data.bookList;
    });
  }

  submit(form: NgForm) {
    if (form.valid) {
      const request = {
        name: this.bookList.name,
        description: this.bookList.description,
        publicAccess: this.bookList.publicAccess,
        bookIds: this.bookList.books.map(book => book.id),
        version: this.bookList.version
      };
      this.bookListService.update(this.bookList.id, request).subscribe(() => {
        this.messageService.showSuccess('Saved book list');
        this.router.navigate(['/lists', this.bookList.id]);
      }, (error: HttpErrorResponse) => this.messageService.showError(`Could not save book list: ${error.status}`));
    }
  }

  remove(bookId: string) {
    const index = this.bookList.books.findIndex(book => book.id === bookId);
    if (index > -1) {
      this.bookList.books.splice(index, 1);
    }
  }

  moveUp(bookId: string) {
    const index = this.bookList.books.findIndex(book => book.id === bookId);
    if (index > 0) {
      BookListEditComponent.arrayMove(this.bookList.books, index, index - 1);
    }
  }

  moveDown(bookId: string) {
    const index = this.bookList.books.findIndex(book => book.id === bookId);
    if (index < this.bookList.books.length - 1) {
      BookListEditComponent.arrayMove(this.bookList.books, index, index + 1);
    }
  }
}
