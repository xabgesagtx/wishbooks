import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EditableBookListItemComponent} from './editable-book-list-item.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {BookListItem} from '../model/book-list-item';
import {AuthorsPipe} from '../../shared/authors.pipe';
import {PublicationDatePipe} from '../../shared/publication-date.pipe';

describe('EditableBookListItemComponent', () => {
  let component: EditableBookListItemComponent;
  let fixture: ComponentFixture<EditableBookListItemComponent>;
  const book: BookListItem = {
    authors: [], googleUrl: '', id: '', language: '', title: ''
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        AuthorsPipe,
        PublicationDatePipe,
        EditableBookListItemComponent
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableBookListItemComponent);
    component = fixture.componentInstance;
    component.book = book;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
