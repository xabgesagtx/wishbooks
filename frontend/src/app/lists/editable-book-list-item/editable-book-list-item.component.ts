import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BookListItem} from '../model/book-list-item';
import {faArrowCircleDown, faArrowCircleUp, faTrashAlt} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-editable-book-list-item',
  templateUrl: './editable-book-list-item.component.html',
  styleUrls: ['./editable-book-list-item.component.scss']
})
export class EditableBookListItemComponent {

  faArrowCircleUp = faArrowCircleUp;
  faArrowCircleDown = faArrowCircleDown;
  faTrashAlt = faTrashAlt;

  @Input()
  book: BookListItem;

  @Input()
  first: boolean;

  @Input()
  last: boolean;

  @Output()
  movedUp = new EventEmitter<string>();

  @Output()
  movedDown = new EventEmitter<string>();

  @Output()
  removed = new EventEmitter<string>();

  constructor() {
  }

  moveUp() {
    this.movedUp.emit(this.book.id);
  }

  moveDown() {
    this.movedDown.emit(this.book.id);
  }

  remove() {
    this.removed.emit(this.book.id);
  }

  get bookClass() {
    if (this.last) {
      return 'book-list-item d-flex';
    } else {
      return 'book-list-item d-flex with-border';
    }
  }

}
