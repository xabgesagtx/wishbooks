import {Injectable} from '@angular/core';
import {BookListService} from './book-list.service';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {BookListSummary} from './model/book-list-summary';
import {EMPTY, Observable} from 'rxjs';
import {catchError, map, take} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class BookListsResolverService implements Resolve<BookListSummary[]> {

  constructor(private bookListService: BookListService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<BookListSummary[]> | Promise<BookListSummary[]> | BookListSummary[] {
    return this.bookListService.getSummaries().pipe(
      take(1),
      map(response => {
        if (response) {
          return response;
        } else {
          this.router.navigate(['/not-found']);
          return null;
        }
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status === 404) {
          this.router.navigate(['/not-found']);
        } else {
          this.router.navigate(['/some-error']);
        }
        return EMPTY as Observable<BookListSummary[]>;
      })
    );
  }
}
