export interface UpdateBookListRequest {
  version: number;
  name: string;
  description: string;
  bookIds: string[];
  publicAccess: boolean;
}
