export interface BookListItem {
  id: string;
  authors: string[];
  title: string;
  publicationDate?: string;
  thumbnailUrl?: string;
  googleUrl: string;
  isbn10?: string;
  isbn13?: string;
  publisher?: string;
  language: string;
}
