export interface CreateBookListRequest {
  name: string;
  description: string;
  publicAccess: boolean;
}
