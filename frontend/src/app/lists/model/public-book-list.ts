import {BookListItem} from './book-list-item';

export interface PublicBookList {
  name: string;
  description: string;
  books: BookListItem[];
}
