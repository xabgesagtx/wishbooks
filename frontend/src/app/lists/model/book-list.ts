import {BookListItem} from './book-list-item';

export interface BookList {
  id: string;
  createdAt: string;
  modifiedAt: string;
  version: number;
  name: string;
  description: string;
  publicAccess: boolean;
  books: BookListItem[];
}
