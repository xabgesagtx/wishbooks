export interface BookListSummary {
  id: string;
  createdAt: string;
  modifiedAt: string;
  name: string;
  description: string;
  bookIds: string[];
}
