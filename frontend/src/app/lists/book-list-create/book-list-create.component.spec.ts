import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookListCreateComponent} from './book-list-create.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {MessageService} from '../../shared/message.service';
import {BookListService} from '../book-list.service';

describe('BookListCreateComponent', () => {
  let component: BookListCreateComponent;
  let fixture: ComponentFixture<BookListCreateComponent>;
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError', 'showSuccess']);
  const bookListServiceSpy = jasmine.createSpyObj('BookListService', ['create']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookListCreateComponent
      ],
      imports: [
        FormsModule,
        RouterTestingModule
      ],
      providers: [
        {provide: MessageService, useValue: messageServiceSpy},
        {provide: BookListService, useValue: bookListServiceSpy},
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
