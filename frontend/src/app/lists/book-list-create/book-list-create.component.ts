import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import {BookListService} from '../book-list.service';
import {Router} from '@angular/router';
import {MessageService} from '../../shared/message.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-book-list-create',
  templateUrl: './book-list-create.component.html',
  styleUrls: ['./book-list-create.component.scss']
})
export class BookListCreateComponent {

  name: string;
  description: string;
  publicAccess = false;

  constructor(private bookListService: BookListService, private router: Router, private messageService: MessageService) {
  }

  submit(form: NgForm) {
    if (form.valid) {
      const request = {
        name: this.name,
        description: this.description,
        publicAccess: this.publicAccess
      };
      this.bookListService.create(request)
        .subscribe(list => {
          this.messageService.showSuccess('Successfully created list');
          this.router.navigate(['lists', list.id]);
        }, (error: HttpErrorResponse) => this.messageService.showError(`Could not create list: ${error.status}`));
    }
  }

}
