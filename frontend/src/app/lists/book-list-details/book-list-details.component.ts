import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BookList} from '../model/book-list';
import {faCopy, faEdit, faShareAlt} from '@fortawesome/free-solid-svg-icons';
import {MessageService} from '../../shared/message.service';

@Component({
  selector: 'app-boot-list-details',
  templateUrl: './book-list-details.component.html',
  styleUrls: ['./book-list-details.component.scss']
})
export class BookListDetailsComponent implements OnInit {

  bookList: BookList;

  faEdit = faEdit;
  faShareAlt = faShareAlt;
  faCopy = faCopy;

  constructor(private messageService: MessageService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: { bookList: BookList }) => {
      this.bookList = data.bookList;
    });
  }

  copyPublicLink() {
    const protocol = window.location.protocol;
    const host = window.location.host;
    const url = `${protocol}//${host}/lists/public/${this.bookList.id}`;
    window.navigator.clipboard.writeText(url).then(() => this.messageService.showSuccess('Copied public link!'));
  }
}
