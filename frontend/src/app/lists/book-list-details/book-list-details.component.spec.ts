import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookListDetailsComponent} from './book-list-details.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {BookList} from '../model/book-list';
import {MessageService} from '../../shared/message.service';

describe('BookListDetailsComponent', () => {
  let component: BookListDetailsComponent;
  let fixture: ComponentFixture<BookListDetailsComponent>;
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showSuccess']);
  const bookList: BookList = {
    books: [],
    createdAt: 'createdAt',
    description: 'description',
    id: 'id',
    modifiedAt: 'modifiedAt',
    name: 'name',
    publicAccess: false,
    version: 0
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookListDetailsComponent
      ],
      providers: [
        {provide: MessageService, useValue: messageServiceSpy},
        {
          provide: ActivatedRoute, useValue: {
            data: of({bookList})
          }
        },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
