import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {catchError, map, take} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {BookList} from './model/book-list';
import {BookListService} from './book-list.service';

@Injectable()
export class BookListResolverService implements Resolve<BookList> {

  constructor(private bookListService: BookListService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BookList> | Promise<BookList> | BookList {
    const id = route.paramMap.get('id');
    return this.bookListService.getBookList(id).pipe(
      take(1),
      map(response => {
        if (response) {
          return response;
        } else {
          this.router.navigate(['/not-found']);
          return null;
        }
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status === 404) {
          this.router.navigate(['/not-found']);
        } else {
          this.router.navigate(['/some-error']);
        }
        return EMPTY as Observable<BookList>;
      })
    );
  }
}
