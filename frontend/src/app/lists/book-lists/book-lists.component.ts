import {Component, OnInit} from '@angular/core';
import {BookListSummary} from '../model/book-list-summary';
import {HttpErrorResponse} from '@angular/common/http';
import {BookListService} from '../book-list.service';
import {MessageService} from '../../shared/message.service';

@Component({
  selector: 'app-book-lists',
  templateUrl: './book-lists.component.html',
  styleUrls: ['./book-lists.component.scss']
})
export class BookListsComponent implements OnInit {

  loading = true;
  bookLists: BookListSummary[] = [];

  constructor(private bookListService: BookListService, private messageService: MessageService) {
  }

  ngOnInit() {
    this.loadBookLists();
  }

  loadBookLists() {
    this.loading = true;
    this.bookListService.getSummaries()
      .subscribe(bookLists => this.bookLists = bookLists,
        (error: HttpErrorResponse) => this.messageService.showError(`Could not load lists: ${error.status}`))
      .add(() => this.loading = false);
  }

  removeList(id: string) {
    const index = this.bookLists.findIndex(list => list.id === id);
    if (index > -1) {
      this.bookLists.splice(index, 1);
    }
  }

}
