import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookListsComponent} from './book-lists.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {BookListService} from '../book-list.service';
import {MessageService} from '../../shared/message.service';
import {of} from 'rxjs';

describe('BookListsComponent', () => {
  let component: BookListsComponent;
  let fixture: ComponentFixture<BookListsComponent>;
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);
  const bookListServiceSpy = jasmine.createSpyObj('BookListService', { getSummaries: of([])});

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookListsComponent
      ],
      providers: [
        {provide: BookListService, useValue: bookListServiceSpy},
        {provide: MessageService, useValue: messageServiceSpy},
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
