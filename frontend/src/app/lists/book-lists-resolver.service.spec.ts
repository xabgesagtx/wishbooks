import {TestBed} from '@angular/core/testing';

import {BookListsResolverService} from './book-lists-resolver.service';
import {RouterTestingModule} from '@angular/router/testing';
import {BookListService} from './book-list.service';

describe('BookListsResolverService', () => {
  const bookListServiceSpy = jasmine.createSpyObj('BookListService', ['getSummaries']);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule
    ],
    providers: [
      {provide: BookListService, useValue: bookListServiceSpy},
      BookListsResolverService
    ]
  }));

  it('should be created', () => {
    const service: BookListsResolverService = TestBed.inject(BookListsResolverService);
    expect(service).toBeTruthy();
  });
});
