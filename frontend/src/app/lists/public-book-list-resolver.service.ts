import {Injectable} from '@angular/core';
import {PublicBookList} from './model/public-book-list';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {catchError, map, take} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {BookListService} from './book-list.service';

@Injectable()
export class PublicBookListResolverService implements Resolve<PublicBookList> {

  constructor(private bookListService: BookListService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<PublicBookList> | Promise<PublicBookList> | PublicBookList {
    const id = route.paramMap.get('id');
    return this.bookListService.getPublicBookList(id).pipe(
      take(1),
      map(response => {
        if (response) {
          return response;
        } else {
          this.router.navigate(['/not-found']);
          return null;
        }
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status === 404) {
          this.router.navigate(['/not-found']);
        } else {
          this.router.navigate(['/some-error']);
        }
        return EMPTY as Observable<PublicBookList>;
      })
    );
  }


}
