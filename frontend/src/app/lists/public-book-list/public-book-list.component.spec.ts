import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {PublicBookListComponent} from './public-book-list.component';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {BookList} from '../model/book-list';

describe('PublicBookListComponent', () => {
  let component: PublicBookListComponent;
  let fixture: ComponentFixture<PublicBookListComponent>;
  const bookList: BookList = {
    books: [],
    createdAt: 'createdAt',
    description: 'description',
    id: 'id',
    modifiedAt: 'modifiedAt',
    name: 'name',
    publicAccess: false,
    version: 0
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        PublicBookListComponent
      ],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            data: of({bookList})
          }
        },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicBookListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
