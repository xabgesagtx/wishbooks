import {Component, OnInit} from '@angular/core';
import {BookList} from '../model/book-list';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-public-book-list',
  templateUrl: './public-book-list.component.html',
  styleUrls: ['./public-book-list.component.scss']
})
export class PublicBookListComponent implements OnInit {

  bookList: BookList;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: { bookList: BookList }) => {
      this.bookList = data.bookList;
    });
  }

}
