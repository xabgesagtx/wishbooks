import {Component, Input} from '@angular/core';
import {BookListItem} from '../model/book-list-item';

@Component({
  selector: 'app-internal-book-list-item',
  templateUrl: './internal-book-list-item.component.html',
  styleUrls: ['./internal-book-list-item.component.scss']
})
export class InternalBookListItemComponent {

  @Input()
  book: BookListItem;

  @Input()
  last: boolean;

  constructor() { }

  get bookClass() {
    if (this.last) {
      return 'book-list-item d-flex';
    } else {
      return 'book-list-item d-flex with-border';
    }
  }


}
