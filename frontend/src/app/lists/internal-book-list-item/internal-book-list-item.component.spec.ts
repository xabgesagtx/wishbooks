import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {InternalBookListItemComponent} from './internal-book-list-item.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AuthorsPipe} from '../../shared/authors.pipe';
import {PublicationDatePipe} from '../../shared/publication-date.pipe';
import {BookListItem} from '../model/book-list-item';
import {LanguagePipe} from '../../shared/language.pipe';

describe('InternalBookListItemComponent', () => {
  let component: InternalBookListItemComponent;
  let fixture: ComponentFixture<InternalBookListItemComponent>;
  const book: BookListItem = {
    authors: [], googleUrl: '', id: '', language: '', title: ''
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AuthorsPipe,
        LanguagePipe,
        PublicationDatePipe,
        InternalBookListItemComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalBookListItemComponent);
    component = fixture.componentInstance;
    component.book = book;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
