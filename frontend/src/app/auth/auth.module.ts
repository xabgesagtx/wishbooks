import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OAuthModule, OAuthResourceServerErrorHandler, OAuthStorage} from 'angular-oauth2-oidc';
import {OAuthErrorRedirectHandler} from './o-auth-error-redirect-handler.service';
import {AuthService} from './auth.service';
import {LoggedInGuard} from './logged-in.guard';


export function storageFactory(): OAuthStorage {
  return localStorage;
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['/api'],
        sendAccessToken: true
      }
    }),
  ],
  providers: [
    AuthService,
    LoggedInGuard,
    {provide: OAuthResourceServerErrorHandler, useClass: OAuthErrorRedirectHandler},
    {provide: OAuthStorage, useFactory: storageFactory}
  ]
})
export class AuthModule {
}
