import {TestBed} from '@angular/core/testing';

import {OAuthErrorRedirectHandler} from './o-auth-error-redirect-handler.service';
import {RouterTestingModule} from '@angular/router/testing';
import {MessageService} from '../shared/message.service';

describe('OAuthErrorRedirectHandlerService', () => {
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule
    ],
    providers: [
      OAuthErrorRedirectHandler,
      {provide: MessageService, useValue: messageServiceSpy},
    ]
  }));

  it('should be created', () => {
    const service: OAuthErrorRedirectHandler = TestBed.inject(OAuthErrorRedirectHandler);
    expect(service).toBeTruthy();
  });
});
