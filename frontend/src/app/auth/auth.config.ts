import {AuthConfig} from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {

  issuer: 'https://dev-canceledsystems.eu.auth0.com/',

  redirectUri: window.location.origin + '/',

  clientId: 'e0FH8Y1IVIZQjrbrDY8Y456stXW38Evq',

  scope: 'openid profile email offline_access wishbooks',

  logoutUrl: 'https://dev-canceledsystems.eu.auth0.com/v2/logout',

  responseType: 'code',

  customQueryParams: {
    audience: 'wishbooks'
  }

};
