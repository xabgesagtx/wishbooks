import {Injectable, NgZone} from '@angular/core';
import {OAuthService} from 'angular-oauth2-oidc';
import {authConfig} from './auth.config';
import {ReplaySubject} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class AuthService {

  private isDoneLoadingSubject$ = new ReplaySubject<boolean>();
  public isDoneLoading$ = this.isDoneLoadingSubject$.asObservable();

  constructor(private zone: NgZone, private oAuthService: OAuthService) {
  }

  configure() {
    this.oAuthService.configure(authConfig);
    this.oAuthService.loadDiscoveryDocument()
      .then(() => this.initialLogin());
    this.oAuthService.setupAutomaticSilentRefresh();

    this.zone.runOutsideAngular(() => {
      setInterval(() => {
        if (!this.oAuthService.hasValidAccessToken() && this.oAuthService.getRefreshToken()) {
          this.zone.run(() => this.oAuthService.refreshToken()
            .catch((err: HttpErrorResponse) => this.handleRefreshTokenError(err))
            .then(() => this.oAuthService.setupAutomaticSilentRefresh())
          );
        }
      }, 3000);
    });

  }

  private initialLogin(): Promise<any> {
    if (this.oAuthService.getRefreshToken()) {
      return this.oAuthService.refreshToken()
        .catch((err: HttpErrorResponse) => this.handleRefreshTokenError(err))
        .finally(() => this.isDoneLoadingSubject$.next(true));
    } else {
      return this.oAuthService.tryLogin().finally(() => this.isDoneLoadingSubject$.next(true));
    }
  }

  get username(): string {
    const claims: any = this.oAuthService.getIdentityClaims();
    if (!claims) {
      return null;
    }

    return claims.name;
  }

  get loggedIn() {
    return this.oAuthService.hasValidIdToken() &&
      this.oAuthService.hasValidAccessToken();
  }

  login() {
    this.oAuthService.initLoginFlow();
  }

  logout() {
    this.oAuthService.revokeTokenAndLogout({
      client_id: this.oAuthService.clientId,
      returnTo: this.oAuthService.redirectUri
    }, true);
  }

  private handleRefreshTokenError(err: HttpErrorResponse) {
    if (err.status >= 400 && err.status < 500) {
      console.log(`Failed to refresh token: ${err.status} - ${err.message}. Logging out`);
      this.logout();
    } else {
      console.log(`Failed to refresh token: ${err.status} - ${err.message}.`);
    }
  }
}
