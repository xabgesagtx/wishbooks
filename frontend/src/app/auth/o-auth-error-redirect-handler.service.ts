import {Injectable} from '@angular/core';
import {OAuthResourceServerErrorHandler} from 'angular-oauth2-oidc';
import {from, Observable, throwError} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {MessageService} from '../shared/message.service';

@Injectable()
export class OAuthErrorRedirectHandler implements OAuthResourceServerErrorHandler {

  constructor(private router: Router, private messageService: MessageService) {
  }

  handleError(err: HttpResponse<any>): Observable<any> {
    if (err.status === 403 || err.status === 401) {
      this.messageService.showError('You are not logged in');
      return from(this.router.navigate(['/']));
    } else {
      return throwError(err);
    }
  }
}
