import {TestBed} from '@angular/core/testing';

import {AuthService} from './auth.service';
import {OAuthService} from 'angular-oauth2-oidc';

describe('AuthService', () => {
  const oAuthServiceSpy = jasmine.createSpyObj('OAuthService', [
    'configure',
    'loadDiscoveryDocument',
    'getRefreshToken',
    'refreshToken',
    'getIdentityClaims',
    'hasValidAccessToken',
    'hasValidIdToken',
    'initLoginFlow',
    'logOut',
  ]);
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      AuthService,
      {provide: OAuthService, useValue: oAuthServiceSpy}
    ]
  }));

  it('should be created', () => {
    const service: AuthService = TestBed.inject(AuthService);
    expect(service).toBeTruthy();
  });
});
