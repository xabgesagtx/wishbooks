import {Pipe, PipeTransform} from '@angular/core';
import {DateTime} from 'luxon';

@Pipe({
  name: 'publicationDate'
})
export class PublicationDatePipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    if (value.indexOf('-') < 0) {
      return value;
    } else {
      const parts = value.split('-');
      if (parts.length === 2) {
        return `${parts[1]}.${parts[0]}`;
      } else {
        const date = new Date(parseInt(parts[0], 10),
          parseInt(parts[1], 10) - 1,
          parseInt(parts[2], 10));
        return DateTime.fromJSDate(date).toFormat('dd.MM.yyyy');
      }
    }
  }

}
