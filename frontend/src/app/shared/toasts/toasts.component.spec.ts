import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ToastsComponent} from './toasts.component';
import {NgbToastModule} from '@ng-bootstrap/ng-bootstrap';
import {MessageService} from '../message.service';

describe('ToastsComponent', () => {
  const messageServiceSpy = jasmine.createSpyObj('MessageService', [], ['toasts']);
  let component: ToastsComponent;
  let fixture: ComponentFixture<ToastsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToastsComponent ],
      imports: [ NgbToastModule ],
      providers: [
        {provide: MessageService, useValue: messageServiceSpy}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ToastsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
