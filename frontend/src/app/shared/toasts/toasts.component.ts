import {Component} from '@angular/core';
import {MessageService} from '../message.service';

@Component({
  selector: 'app-toasts',
  templateUrl: './toasts.component.html',
  styleUrls: ['./toasts.component.scss']
})
export class ToastsComponent {

  constructor(public toastService: MessageService) {

  }
}
