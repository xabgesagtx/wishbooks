import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MessageService} from './message.service';
import {LoadingSpinnerComponent} from './loading-spinner/loading-spinner.component';
import {AuthorsPipe} from './authors.pipe';
import {PublicationDatePipe} from './publication-date.pipe';
import {LanguagePipe} from './language.pipe';
import {RelativeDatePipe} from './relative-date.pipe';
import {ToastsComponent} from './toasts/toasts.component';
import {NgbToastModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    LoadingSpinnerComponent,
    AuthorsPipe,
    PublicationDatePipe,
    LanguagePipe,
    RelativeDatePipe,
    ToastsComponent
  ],
  imports: [
    CommonModule,
    NgbToastModule
  ],
  exports: [
    LoadingSpinnerComponent,
    AuthorsPipe,
    PublicationDatePipe,
    LanguagePipe,
    RelativeDatePipe,
    ToastsComponent
  ],
  providers: [
    MessageService
  ]
})
export class SharedModule { }
