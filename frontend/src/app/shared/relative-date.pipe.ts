import {Pipe, PipeTransform} from '@angular/core';
import {DateTime} from 'luxon';

@Pipe({
  name: 'relativeDate'
})
export class RelativeDatePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): string {
    if (value instanceof DateTime) {
      return this.formatDateTime(value);
    } else if (value instanceof Date) {
      return this.formatDateTime(DateTime.fromJSDate(value));
    } else if (typeof value === 'string') {
      return this.formatDateTime(DateTime.fromISO(value));
    } else {
      return `Unknown date type ${typeof value}`;
    }
  }

  private formatDateTime(dateTime: DateTime): string {
    if (dateTime.diffNow().as('days') < -7) {
      return dateTime.toFormat('dd.MM.yyyy');
    } else {
      return dateTime.toRelative({locale: 'en-US'});
    }
  }

}
