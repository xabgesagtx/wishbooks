import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'authors'
})
export class AuthorsPipe implements PipeTransform {

  transform(value: string[], ...args: any[]): string {
    if (value.length === 0) {
      return 'Unknown author';
    } else {
      return value.join(', ');
    }
  }

}
