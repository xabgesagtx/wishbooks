import {Injectable} from '@angular/core';
import {Toast} from './model/toast';

@Injectable()
export class MessageService {

  public toasts: Toast[] = [];

  constructor() {
  }

  showError(message: string) {
    this.show(message, false);
  }

  showSuccess(message: string) {
    this.show(message, true);
  }

  private show(message: string, success: boolean) {
    const classname = success ? 'bg-success text-light' : 'bg-danger text-light';
    this.toasts.push({message, classname});
  }

  remove(toast: Toast) {
    this.toasts = this.toasts.filter((t) => t !== toast);
  }

}
