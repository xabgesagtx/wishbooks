import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './main/home/home.component';
import {NotFoundComponent} from './main/not-found/not-found.component';
import {MainModule} from './main/main.module';
import {SearchRoutingModule} from './search/search-routing.module';
import {BooksRoutingModule} from './books/books-routing.module';
import {ListsRoutingModule} from './lists/lists-routing.module';
import {ReviewRoutingModule} from './review/review-routing.module';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [
    MainModule,
    SearchRoutingModule,
    BooksRoutingModule,
    ListsRoutingModule,
    ReviewRoutingModule,
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
