import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.scss']
})
export class AuthorsComponent {

  @Input()
  authors: string[];

  @Output()
  authorsChanged = new EventEmitter<string[]>();

  constructor() { }

  trackByIdx(index: number) {
    return index;
  }

  remove(index: number) {
    this.authors.splice(index, 1);
  }

  add() {
    this.authors.push('');
  }

  emitChange() {
    this.authorsChanged.emit(this.authors);
  }
}
