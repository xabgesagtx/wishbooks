import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DeleteBookComponent} from './delete-book.component';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BookService} from '../book.service';
import {MessageService} from '../../shared/message.service';
import {BookStatus} from '../model/book-status';

describe('DeleteBookComponent', () => {
  let component: DeleteBookComponent;
  let fixture: ComponentFixture<DeleteBookComponent>;
  const ngbActiveModalSpy = jasmine.createSpyObj('NgbActiveModal', ['close', 'dimiss']);
  const bookServiceSpy = jasmine.createSpyObj('BookService', ['remove']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteBookComponent ],
      providers: [
        {provide: NgbActiveModal, useValue: ngbActiveModalSpy},
        {provide: BookService, useValue: bookServiceSpy},
        {provide: MessageService, useValue: messageServiceSpy},
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteBookComponent);
    component = fixture.componentInstance;
    component.book = {
      authors: [],
      createdAt: '1970-01-01T00:00:00.000',
      externalId: 'externalId',
      id: 'id',
      language: 'en',
      modifiedAt: '1970-01-01T00:00:00.000',
      status: BookStatus.WANTED,
      title: 'title',
      version: 0
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
