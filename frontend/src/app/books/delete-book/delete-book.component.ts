import {Component, Input} from '@angular/core';
import {Book} from '../model/book';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BookService} from '../book.service';
import {MessageService} from '../../shared/message.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-delete-book',
  templateUrl: './delete-book.component.html',
  styleUrls: ['./delete-book.component.scss']
})
export class DeleteBookComponent {

  @Input() book: Book;

  constructor(private activeModal: NgbActiveModal,
              private bookService: BookService,
              private messageService: MessageService) { }

  remove() {
    this.bookService.remove(this.book.id)
      .subscribe(() => this.activeModal.close(),
        (error: HttpErrorResponse) => this.messageService.showError(`Failed to remove book: ${error.status} - ${error.message}`));
  }

  dismiss() {
    this.activeModal.dismiss();
  }
}
