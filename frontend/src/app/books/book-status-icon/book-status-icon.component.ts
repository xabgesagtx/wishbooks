import {Component, Input} from '@angular/core';
import {faCheckCircle, faShoppingCart, faBookOpen} from '@fortawesome/free-solid-svg-icons';
import {BookStatus} from '../model/book-status';

@Component({
  selector: 'app-book-status-icon',
  templateUrl: './book-status-icon.component.html',
  styleUrls: ['./book-status-icon.component.scss']
})
export class BookStatusIconComponent {

  @Input()
  status: BookStatus;

  constructor() { }

  get icon() {
    switch (this.status) {
      case BookStatus.WANTED:
        return faShoppingCart;
      case BookStatus.BOUGHT:
        return faBookOpen;
      case BookStatus.READ:
        return faCheckCircle;
    }
  }

  get title() {
    switch (this.status) {
      case BookStatus.WANTED:
        return 'You want this book';
      case BookStatus.BOUGHT:
        return 'You have this book and want to read it';
      case BookStatus.READ:
        return 'Done. Bought and read';
    }
  }

}
