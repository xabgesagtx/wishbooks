import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookStatusIconComponent} from './book-status-icon.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('BookStatusIconComponent', () => {
  let component: BookStatusIconComponent;
  let fixture: ComponentFixture<BookStatusIconComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookStatusIconComponent
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStatusIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
