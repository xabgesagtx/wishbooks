import {BookStatus} from './book-status';

export class Book {
  id: string;
  externalId: string;
  createdAt: string;
  modifiedAt: string;
  version: number;
  status: BookStatus;
  boughtAt?: string;
  readAt?: string;
  title: string;
  authors: string[];
  publicationDate?: string;
  description?: string;
  notes?: string;
  thumbnailUrl?: string;
  googleUrl?: string;
  isbn10?: string;
  isbn13?: string;
  language: string;
  pageCount?: number;
  publisher?: string;
}
