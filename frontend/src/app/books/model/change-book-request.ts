import {BookStatus} from './book-status';

export class ChangeBookRequest {
  status: BookStatus;
  version: number;
  title: string;
  authors: string[];
  publicationDate?: string;
  description?: string;
  notes?: string;
  isbn10?: string;
  isbn13?: string;
  language: string;
  pageCount?: number;
  publisher?: string;
}
