export enum BookStatus {
  WANTED = 'WANTED',
  BOUGHT = 'BOUGHT',
  READ = 'READ'
}
