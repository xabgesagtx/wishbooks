import {TestBed} from '@angular/core/testing';

import {BookResolverService} from './book-resolver.service';
import {RouterTestingModule} from '@angular/router/testing';
import {BookService} from './book.service';

describe('BookResolverService', () => {
  const bookServiceSpy = jasmine.createSpyObj('BookService', ['get']);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule
    ],
    providers: [
      BookResolverService,
      {provide: BookService, useValue: bookServiceSpy}
    ]
  }));

  it('should be created', () => {
    const service: BookResolverService = TestBed.inject(BookResolverService);
    expect(service).toBeTruthy();
  });
});
