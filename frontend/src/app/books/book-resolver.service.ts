import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Book} from './model/book';
import {EMPTY, Observable} from 'rxjs';
import {catchError, map, take} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {BookService} from './book.service';

@Injectable()
export class BookResolverService implements Resolve<Book> {

  constructor(private bookService: BookService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Book> | Promise<Book> | Book {
    const id = route.paramMap.get('id');
    return this.bookService.get(id).pipe(
      take(1),
      map(response => {
        if (response) {
          return response;
        } else {
          this.router.navigate(['/not-found']);
          return null;
        }
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status === 404) {
          this.router.navigate(['/not-found']);
        } else {
          this.router.navigate(['/some-error']);
        }
        return EMPTY as Observable<Book>;
      })
    );
  }
}
