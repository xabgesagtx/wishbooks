import {Component, OnInit} from '@angular/core';
import {Book} from '../model/book';
import {BookService} from '../book.service';
import {ChangeBookRequest} from '../model/change-book-request';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from '../../shared/message.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.scss']
})
export class BookEditComponent implements OnInit {

  book: Book;

  constructor(private bookService: BookService,
              private router: Router,
              private messageService: MessageService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe((data: {book: Book}) => {
      this.book = data.book;
    });
  }

  save() {
    const request: ChangeBookRequest = {
      status: this.book.status,
      version: this.book.version,
      title: this.book.title,
      authors: this.book.authors,
      publicationDate: this.book.publicationDate,
      description: this.book.description,
      notes: this.book.notes,
      isbn10: this.book.isbn10,
      isbn13: this.book.isbn13,
      language: this.book.language,
      pageCount: this.book.pageCount,
      publisher: this.book.publisher
    };
    this.bookService.update(this.book.id, request).subscribe(
      () => {
        this.router.navigate([`/books/${this.book.id}`]);
        this.messageService.showSuccess('Save book');
      },
      (error: HttpErrorResponse) => this.messageService.showError(`Could not save book: ${error.status}`)
    );
  }
}
