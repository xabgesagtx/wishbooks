import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookEditComponent} from './book-edit.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MessageService} from '../../shared/message.service';
import {BookService} from '../book.service';
import {FormsModule} from '@angular/forms';
import {Book} from '../model/book';
import {BookStatus} from '../model/book-status';
import {ActivatedRoute, Router} from '@angular/router';
import {of} from 'rxjs';

describe('BookEditComponent', () => {
  const book: Book = {
    authors: [],
    createdAt: '1970-01-01T00:00:00.000',
    externalId: 'externalId',
    id: 'id',
    language: 'en',
    modifiedAt: '1970-01-01T00:00:00.000',
    status: BookStatus.WANTED,
    title: 'title',
    version: 0
  };
  let component: BookEditComponent;
  let fixture: ComponentFixture<BookEditComponent>;
  const bookServiceSpy = jasmine.createSpyObj('BookService', ['update']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError', 'showSuccess']);
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookEditComponent
      ],
      imports: [
        FormsModule
      ],
      providers: [
        {provide: BookService, useValue: bookServiceSpy},
        {provide: MessageService, useValue: messageServiceSpy},
        {
          provide: ActivatedRoute, useValue: {
            data: of({book})
          }
        },
        {provide: Router, useValue: routerSpy}
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
