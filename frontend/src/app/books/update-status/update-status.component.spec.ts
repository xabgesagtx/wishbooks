import {ComponentFixture, TestBed} from '@angular/core/testing';

import {UpdateStatusComponent} from './update-status.component';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BookService} from '../book.service';
import {MessageService} from '../../shared/message.service';

describe('UpdateStatusComponent', () => {
  let component: UpdateStatusComponent;
  let fixture: ComponentFixture<UpdateStatusComponent>;
  const ngbActiveModalSpy = jasmine.createSpyObj('NgbActiveModal', ['close', 'dimiss']);
  const bookServiceSpy = jasmine.createSpyObj('BookService', ['updateStatus']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateStatusComponent ],
      providers: [
        {provide: NgbActiveModal, useValue: ngbActiveModalSpy},
        {provide: BookService, useValue: bookServiceSpy},
        {provide: MessageService, useValue: messageServiceSpy},
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
