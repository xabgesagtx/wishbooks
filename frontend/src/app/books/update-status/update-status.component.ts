import {Component, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BookService} from '../book.service';
import {BookStatus} from '../model/book-status';
import {HttpErrorResponse} from '@angular/common/http';
import {MessageService} from '../../shared/message.service';

@Component({
  selector: 'app-update-status',
  templateUrl: './update-status.component.html',
  styleUrls: ['./update-status.component.scss'],
  preserveWhitespaces: true
})
export class UpdateStatusComponent {

  @Input() id: string;
  @Input() status: BookStatus;

  bought = BookStatus.BOUGHT;
  read = BookStatus.READ;
  wanted = BookStatus.WANTED;

  constructor(private activeModal: NgbActiveModal,
              private bookService: BookService,
              private messageService: MessageService) {
  }

  dismiss() {
    this.activeModal.dismiss();
  }

  save() {
    this.bookService.updateStatus(this.id, this.status)
      .subscribe(() => this.activeModal.close(this.status),
        (error: HttpErrorResponse) => this.messageService.showError(`Failed to update status: ${error.status} - ${error.message}`));
  }

}
