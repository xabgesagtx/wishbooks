import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookDetailsComponent} from './book-details.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AuthorsPipe} from '../../shared/authors.pipe';
import {PublicationDatePipe} from '../../shared/publication-date.pipe';
import {ActivatedRoute, Router} from '@angular/router';
import {of} from 'rxjs';
import {Book} from '../model/book';
import {BookStatus} from '../model/book-status';
import {BookService} from '../book.service';
import {MessageService} from '../../shared/message.service';
import {LanguagePipe} from '../../shared/language.pipe';
import {SharedModule} from '../../shared/shared.module';

describe('BookDetailsComponent', () => {

  const book: Book = {
    authors: [],
    createdAt: '1970-01-01T00:00:00.000',
    externalId: 'externalId',
    id: 'id',
    language: 'en',
    modifiedAt: '1970-01-01T00:00:00.000',
    status: BookStatus.WANTED,
    title: 'title',
    version: 0
  };
  let component: BookDetailsComponent;
  let fixture: ComponentFixture<BookDetailsComponent>;
  const bookServiceSpy = jasmine.createSpyObj('BookService', ['remove']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError', 'showSuccess']);
  const routerServiceSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookDetailsComponent,
        AuthorsPipe,
        LanguagePipe,
        PublicationDatePipe
      ],
      imports: [
        SharedModule
      ],
      providers: [
        {provide: BookService, useValue: bookServiceSpy},
        {provide: MessageService, useValue: messageServiceSpy},
        {provide: Router, useValue: routerServiceSpy},
        {
          provide: ActivatedRoute, useValue: {
            data: of({book})
          }
        },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
