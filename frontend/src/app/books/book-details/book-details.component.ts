import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Book} from '../model/book';
import {faEdit, faExternalLinkAlt, faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import {BookService} from '../book.service';
import {MessageService} from '../../shared/message.service';
import {UpdateStatusComponent} from '../update-status/update-status.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DeleteBookComponent} from '../delete-book/delete-book.component';
import {BookStatus} from '../model/book-status';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.scss'],
  preserveWhitespaces: true
})
export class BookDetailsComponent implements OnInit {

  book: Book;
  faExternalLinkAlt = faExternalLinkAlt;
  faEdit = faEdit;
  faTrashAlt = faTrashAlt;

  constructor(private bookService: BookService,
              private messageService: MessageService,
              private route: ActivatedRoute,
              private router: Router,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.route.data.subscribe((data: {book: Book}) => {
      this.book = data.book;
    });
  }

  get createdAt() {
    return new Date(this.book.createdAt);
  }

  get modifiedAt() {
    return new Date(this.book.modifiedAt);
  }

  remove() {
    const modalRef = this.modalService.open(DeleteBookComponent);
    modalRef.componentInstance.book = this.book;
    modalRef.result.then(() => {
      this.messageService.showSuccess(`Removed "${this.book.title}"`);
      this.router.navigate(['/books']);
    }, () => {/* do nothing */});
  }

  updateStatus() {
    const modalRef = this.modalService.open(UpdateStatusComponent);
    modalRef.componentInstance.id = this.book.id;
    modalRef.componentInstance.status = this.book.status;
    modalRef.result.then(status => {
      if (status === BookStatus.READ) {
        this.book.readAt = new Date().toISOString();
      } else if (status === BookStatus.BOUGHT) {
        this.book.boughtAt = new Date().toISOString();
      }
      this.book.status = status;
      this.book.modifiedAt = new Date().toISOString();
    }, () => {/* do nothing */});
  }

}
