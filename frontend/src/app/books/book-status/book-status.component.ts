import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BookStatus} from '../model/book-status';

@Component({
  selector: 'app-book-status',
  templateUrl: './book-status.component.html',
  styleUrls: ['./book-status.component.scss']
})
export class BookStatusComponent {

  @Input()
  status: BookStatus;

  @Output()
  statusChanged = new EventEmitter<BookStatus>();

  constructor() { }

  emitChange() {
    this.statusChanged.emit(this.status);
  }

  get statusValues(): BookStatus[] {
    return Object.keys(BookStatus).map(key => BookStatus[key]);
  }

}
