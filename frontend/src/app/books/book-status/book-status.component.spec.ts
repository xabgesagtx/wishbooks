import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookStatusComponent} from './book-status.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('BookStatusComponent', () => {
  let component: BookStatusComponent;
  let fixture: ComponentFixture<BookStatusComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookStatusComponent
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
