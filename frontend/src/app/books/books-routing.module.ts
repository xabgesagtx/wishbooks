import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoggedInGuard} from '../auth/logged-in.guard';
import {BookResolverService} from './book-resolver.service';
import {BookListComponent} from './book-list/book-list.component';
import {BookDetailsComponent} from './book-details/book-details.component';
import {BookEditComponent} from './book-edit/book-edit.component';
import {BookListsResolverService} from '../lists/book-lists-resolver.service';


const routes: Routes = [
  { path: 'books', component: BookListComponent, canActivate: [LoggedInGuard], resolve: { bookLists: BookListsResolverService} },
  { path: 'books/:id', component: BookDetailsComponent, canActivate: [LoggedInGuard], resolve: {book: BookResolverService}},
  { path: 'books/:id/edit', component: BookEditComponent, canActivate: [LoggedInGuard], resolve: {book: BookResolverService}}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {})
  ],
  exports: [RouterModule]
})
export class BooksRoutingModule { }
