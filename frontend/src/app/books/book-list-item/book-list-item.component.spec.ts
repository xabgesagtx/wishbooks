import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookListItemComponent} from './book-list-item.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AuthorsPipe} from '../../shared/authors.pipe';
import {PublicationDatePipe} from '../../shared/publication-date.pipe';
import {MessageService} from '../../shared/message.service';
import {BookStatus} from '../model/book-status';
import {BookListService} from '../../lists/book-list.service';

describe('BookListItemComponent', () => {
  let component: BookListItemComponent;
  let fixture: ComponentFixture<BookListItemComponent>;
  const bookListServiceSpy = jasmine.createSpyObj('BookListService', ['addBook', 'removeBook']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError', 'showSuccess']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookListItemComponent,
        AuthorsPipe,
        PublicationDatePipe
      ],
      providers: [
        {provide: BookListService, useValue: bookListServiceSpy},
        {provide: MessageService, useValue: messageServiceSpy},
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListItemComponent);
    component = fixture.componentInstance;
    component.book = {
      authors: [],
      createdAt: '1970-01-01T00:00:00.000',
      externalId: 'externalId',
      id: 'id',
      language: 'en',
      modifiedAt: '1970-01-01T00:00:00.000',
      status: BookStatus.WANTED,
      title: 'title',
      version: 0
    };
    component.bookLists = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
