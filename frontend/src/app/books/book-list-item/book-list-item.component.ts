import {Component, EventEmitter, Input, Output, TemplateRef} from '@angular/core';
import {Book} from '../model/book';
import {HttpErrorResponse} from '@angular/common/http';
import {MessageService} from '../../shared/message.service';
import {faCommentAlt, faEdit, faMinusCircle, faPlusCircle, faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {BookListSummary} from '../../lists/model/book-list-summary';
import {BookListService} from '../../lists/book-list.service';
import {UpdateStatusComponent} from '../update-status/update-status.component';
import {DeleteBookComponent} from '../delete-book/delete-book.component';
import {BookStatus} from '../model/book-status';

@Component({
  selector: 'app-book-list-item',
  templateUrl: './book-list-item.component.html',
  styleUrls: ['./book-list-item.component.scss']
})
export class BookListItemComponent {

  @Input()
  book: Book;

  @Input()
  bookLists: BookListSummary[];

  @Input()
  last: boolean;

  @Output()
  bookRemoved = new EventEmitter<string>();

  @Output()
  removedFromList = new EventEmitter<string>();

  @Output()
  addedToList = new EventEmitter<string>();

  faEdit = faEdit;
  faTrashAlt = faTrashAlt;
  faComment = faCommentAlt;
  faPlusCircle = faPlusCircle;
  faMinusCircle = faMinusCircle;

  constructor(private bookListService: BookListService,
              private messageService: MessageService,
              private modalService: NgbModal) {
  }

  get bookClass() {
    if (this.last) {
      return 'book-list-item d-flex';
    } else {
      return 'book-list-item d-flex with-border';
    }
  }

  remove() {
    const modalRef = this.modalService.open(DeleteBookComponent);
    modalRef.componentInstance.book = this.book;
    modalRef.result.then(() => {
      this.messageService.showSuccess(`Removed "${this.book.title}"`);
      this.bookRemoved.emit(this.book.id);
    }, () => {/* do nothing */});
  }

  open(notes: TemplateRef<any>) {
    this.modalService.open(notes);
  }

  get listNamesIncluded() {
    return this.bookLists.filter(list => list.bookIds.indexOf(this.book.id) > -1)
      .map(list => list.name);
  }

  get listsIncluded() {
    return this.bookLists.filter(list => list.bookIds.indexOf(this.book.id) > -1);
  }

  get listsExcluded() {
    return this.bookLists.filter(list => list.bookIds.indexOf(this.book.id) === -1);
  }

  addToList(listId: string) {
    this.bookListService.addBook(listId, this.book.id).subscribe(() => {
      this.addedToList.emit(listId);
      this.messageService.showSuccess('Added book to list');
    }, (error: HttpErrorResponse) => this.messageService.showError(`Could not add book to list: ${error.status}`));
  }

  removeFromList(listId: string) {
    this.bookListService.removeBook(listId, this.book.id).subscribe(() => {
      this.removedFromList.emit(listId);
      this.messageService.showSuccess('Removed book from list');
    }, (error: HttpErrorResponse) => this.messageService.showError(`Could not remove book from list: ${error.status}`));
  }

  updateStatus() {
    const modalRef = this.modalService.open(UpdateStatusComponent);
    modalRef.componentInstance.id = this.book.id;
    modalRef.componentInstance.status = this.book.status;
    modalRef.result.then(status => {
      if (status === BookStatus.READ) {
        this.book.readAt = new Date().toISOString();
      } else if (status === BookStatus.BOUGHT) {
        this.book.boughtAt = new Date().toISOString();
      }
      this.book.status = status;
      this.book.modifiedAt = new Date().toISOString();
    }, () => {/* do nothing */});
  }
}
