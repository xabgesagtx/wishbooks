import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Book} from './model/book';
import {ChangeBookRequest} from './model/change-book-request';
import {BookStatus} from './model/book-status';

@Injectable()
export class BookService {

  constructor(private httpClient: HttpClient) {
  }

  get(id: string) {
    return this.httpClient.get<Book>(`/api/books/${id}`);
  }

  findAll() {
    return this.httpClient.get<Book[]>('/api/books');
  }

  update(id: string, request: ChangeBookRequest) {
    return this.httpClient.put<Book>(`/api/books/${id}`, request);
  }

  updateStatus(id: string, status: BookStatus) {
    const params = new HttpParams().append('status', status);
    return this.httpClient.put<Book>(`/api/books/${id}/status`, null, {params});
  }

  add(externalId: string) {
    const params = new HttpParams().append('externalId', externalId);
    return this.httpClient.post<Book>('/api/books/add', null, {params});
  }

  remove(id: string) {
    return this.httpClient.delete<Book>(`/api/books/${id}`);
  }

  yearForReadBooks() {
    return this.httpClient.get<number[]>('/api/books/years/read');
  }

  booksReadInYear(year: number) {
    return this.httpClient.get<Book[]>(`/api/books/read/${year}`);
  }

}
