import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BookListComponent} from './book-list/book-list.component';
import {BookListItemComponent} from './book-list-item/book-list-item.component';
import {BookDetailsComponent} from './book-details/book-details.component';
import {BookEditComponent} from './book-edit/book-edit.component';
import {AuthorsComponent} from './authors/authors.component';
import {BookStatusComponent} from './book-status/book-status.component';
import {FormsModule} from '@angular/forms';
import {BookService} from './book.service';
import {BookResolverService} from './book-resolver.service';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BookStatusIconComponent} from './book-status-icon/book-status-icon.component';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {UpdateStatusComponent} from './update-status/update-status.component';
import {DeleteBookComponent} from './delete-book/delete-book.component';


@NgModule({
    declarations: [
        BookListComponent,
        BookListItemComponent,
        BookDetailsComponent,
        BookEditComponent,
        AuthorsComponent,
        BookStatusComponent,
        BookStatusIconComponent,
        UpdateStatusComponent,
        DeleteBookComponent
    ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule,
    FontAwesomeModule,
    NgbDropdownModule,
  ],
    exports: [
        BookListItemComponent
    ],
    providers: [
        BookService,
        BookResolverService
    ]
})
export class BooksModule {
}
