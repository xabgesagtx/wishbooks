import {Component, OnInit} from '@angular/core';
import {Book} from '../model/book';
import {BookService} from '../book.service';
import {MessageService} from '../../shared/message.service';
import {HttpErrorResponse} from '@angular/common/http';
import {BookStatus} from '../model/book-status';
import {ActivatedRoute} from '@angular/router';
import {BookListSummary} from '../../lists/model/book-list-summary';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {
  books: Book[] = [];
  bookLists: BookListSummary[] = [];
  loading = true;
  bookFilter = 'wanted';
  sortBy = 'createdAt';

  constructor(private bookService: BookService, private messageService: MessageService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: { bookLists: BookListSummary[] }) => {
      this.bookLists = data.bookLists;
    });
    this.loadSettings();
    this.loadBooks();
  }

  get booksFiltered() {
    return this.books.filter(book => this.filter(book))
      .sort((bookA, bookB) => this.sort(bookA, bookB));
  }

  filterChanged() {
    localStorage.setItem('bookFilter', this.bookFilter);
  }

  sortByChanged() {
    localStorage.setItem('sortBy', this.sortBy);
  }

  filter(book: Book) {
    if (this.bookFilter === 'wanted') {
      return book.status === BookStatus.WANTED;
    } else if (this.bookFilter === 'bought') {
      return book.status === BookStatus.BOUGHT;
    } else if (this.bookFilter === 'read') {
      return book.status === BookStatus.READ;
    } else {
      return true;
    }
  }

  sort(bookA: Book, bookB: Book) {
    if (this.sortBy === 'statusChange') {
      if (this.bookFilter === 'read') {
        return bookB.readAt.localeCompare(bookA.readAt);
      } else if (this.bookFilter === 'bought') {
        return bookB.boughtAt.localeCompare(bookA.boughtAt);
      } else if (this.bookFilter === 'wanted') {
        return bookB.createdAt.localeCompare(bookA.createdAt);
      } else {
        return bookB.modifiedAt.localeCompare(bookA.modifiedAt);
      }
    } else if (this.sortBy === 'createdAt') {
      return bookB.createdAt.localeCompare(bookA.createdAt);
    } else if (this.sortBy === 'title') {
      return bookA.title.localeCompare(bookB.title);
    } else {
      const publicationDateA = bookA.publicationDate ? bookA.publicationDate : '';
      const publicationDateB = bookB.publicationDate ? bookB.publicationDate : '';
      return publicationDateB.localeCompare(publicationDateA);
    }
  }

  loadBooks() {
    this.loading = true;
    this.bookService.findAll()
      .subscribe(books => this.books = books,
        (error: HttpErrorResponse) => this.messageService.showError(`Could not load books: ${error.status}`))
      .add(() => this.loading = false);
  }

  private loadSettings() {
    const bookFilter = localStorage.getItem('bookFilter');
    if (bookFilter) {
      this.bookFilter = bookFilter;
    }
    const sortBy = localStorage.getItem('sortBy');
    if (sortBy) {
      this.sortBy = sortBy;
    }
  }

  addToList(listId: string, bookId: string) {
    const bookList = this.bookLists.find(list => list.id === listId);
    if (bookList) {
      const index = bookList.bookIds.indexOf(bookId);
      if (index === -1) {
        bookList.bookIds.push(bookId);
      }
    }
  }

  removeFromList(listId: string, bookId: string) {
    const bookList = this.bookLists.find(list => list.id === listId);
    if (bookList) {
      const index = bookList.bookIds.indexOf(bookId);
      if (index > -1) {
        bookList.bookIds.splice(index, 1);
      }
    }
  }

}
