import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BookListComponent} from './book-list.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {BookService} from '../book.service';
import {MessageService} from '../../shared/message.service';
import {of} from 'rxjs';
import {BookList} from '../../lists/model/book-list';
import {ActivatedRoute} from '@angular/router';

describe('BookListComponent', () => {
  let component: BookListComponent;
  let fixture: ComponentFixture<BookListComponent>;
  const bookList: BookList = {
    books: [], createdAt: '', description: '', id: '', modifiedAt: '', name: '', publicAccess: false, version: 0
  };
  const bookServiceSpy = jasmine.createSpyObj('BookService', {
    findAll: of([])
  });
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookListComponent
      ],
      providers: [
        {provide: BookService, useValue: bookServiceSpy},
        {provide: MessageService, useValue: messageServiceSpy},
        {provide: ActivatedRoute, useValue: {
            data: of({bookList})
          }
        }
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
