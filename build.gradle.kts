import com.github.gradle.node.npm.task.NpmTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
	id("com.github.node-gradle.node") version "7.0.2"
	id("org.springframework.boot") version "3.3.2"
	id("io.spring.dependency-management") version "1.1.6"
	kotlin("jvm") version "1.9.24"
	kotlin("plugin.spring") version "1.9.24"
}

java {
	toolchain {
		languageVersion = JavaLanguageVersion.of(17)
	}
}

repositories {
	mavenCentral()
}

node {
	version.set("18.19.0")
	npmVersion.set("10.2.3")
	nodeProjectDir.set(file("${projectDir}/frontend"))
	download.set(true)
}

tasks.register<NpmTask>("runLint") {
	workingDir.set(file("${projectDir}/frontend"))
	args.set(listOf("run", "lint"))
	dependsOn(":npmInstall")
}

tasks.register<NpmTask>("runKarma") {
	workingDir.set(file("${projectDir}/frontend"))
	args.set(listOf("run", "test-headless"))
	dependsOn(":npmInstall")
}

tasks.register<NpmTask>("buildAngular") {
	workingDir.set(file("${projectDir}/frontend"))
	args.set(listOf("run", "build"))
	dependsOn(":npmInstall")
}

tasks.withType<BootJar> {
	if (!project.hasProperty("skipNode")) {
		dependsOn(":buildAngular")
	}
	from("frontend/dist/frontend") {
		into("public")
	}
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
	implementation("org.springframework.security:spring-security-oauth2-resource-server")
	implementation("org.springframework.security:spring-security-oauth2-jose")
	implementation("io.micrometer:micrometer-registry-prometheus")
	implementation("com.google.apis:google-api-services-books:v1-rev20240214-2.0.0")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("org.springframework.boot:spring-boot-testcontainers")
	testImplementation("org.testcontainers:mongodb")
	testImplementation("org.mockito.kotlin:mockito-kotlin:5.4.0")
	annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "17"
	}
}
