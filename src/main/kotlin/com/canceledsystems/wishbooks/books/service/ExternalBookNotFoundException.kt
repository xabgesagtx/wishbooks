package com.canceledsystems.wishbooks.books.service

import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(NOT_FOUND)
class ExternalBookNotFoundException(externalId: String): RuntimeException("Book with externalId $externalId could not be found")