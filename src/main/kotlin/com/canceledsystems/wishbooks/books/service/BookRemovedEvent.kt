package com.canceledsystems.wishbooks.books.service

data class BookRemovedEvent(val id: String)