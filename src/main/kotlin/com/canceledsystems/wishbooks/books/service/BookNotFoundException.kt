package com.canceledsystems.wishbooks.books.service

import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(NOT_FOUND)
class BookNotFoundException(id: String): RuntimeException("Book with $id could not be found")