package com.canceledsystems.wishbooks.books.service

import com.canceledsystems.wishbooks.books.common.BookStatus

data class UpdateBookRequest(val status: BookStatus,
                             val version: Int,
                             val title: String,
                             val authors: List<String>,
                             val publicationDate: String? = null,
                             val description: String? = null,
                             val notes: String? = null,
                             val isbn10: String? = null,
                             val isbn13: String? = null,
                             val language: String,
                             val pageCount: Int? = null,
                             val publisher: String? = null)