package com.canceledsystems.wishbooks.books.service

import com.canceledsystems.wishbooks.books.common.BookStatus
import com.canceledsystems.wishbooks.books.persistence.BookDocument
import com.canceledsystems.wishbooks.books.persistence.BookRepository
import com.canceledsystems.wishbooks.books.persistence.SecureBookRepository
import com.canceledsystems.wishbooks.booksapi.BookApiClient
import com.canceledsystems.wishbooks.booksapi.GoogleVolume
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import java.time.Clock
import java.time.ZoneId
import java.time.ZonedDateTime

@Service
class BookService(
    private val bookRepository: SecureBookRepository,
    private val insecureBookRepository: BookRepository,
    private val booksApiClient: BookApiClient,
    private val eventPublisher: ApplicationEventPublisher,
    private val clock: Clock = Clock.systemUTC()
) {

    private val log = LoggerFactory.getLogger(javaClass)

    fun add(externalId: String): Book {
        log.info("Adding book with externalId $externalId")
        val details = booksApiClient.details(externalId) ?: throw ExternalBookNotFoundException(externalId)
        return details.toBookDocument()
            .let { bookRepository.save(it) }
            .toBook()
    }

    fun update(id: String, request: UpdateBookRequest): Book {
        log.info("Updating book $id")
        val book = bookRepository.findById(id) ?: throw BookNotFoundException(id)
        return book.update(request)
            .let { bookRepository.save(it) }
            .toBook()
    }

    fun updateStatus(id: String, status: BookStatus) {
        log.info("Updating status of book $id to $status")
        val book = bookRepository.findById(id) ?: throw BookNotFoundException(id)
        val updatedBook = when (status) {
            BookStatus.BOUGHT -> book.copy(status = status, boughtAt = clock.instant())
            BookStatus.READ -> book.copy(status = status, readAt = clock.instant())
            BookStatus.WANTED -> book.copy(status = status)
        }
        bookRepository.save(updatedBook)
    }

    fun get(id: String): Book? {
        log.info("Retrieving book $id")
        return bookRepository.findById(id)?.toBook()
    }

    fun findAll(): List<Book> {
        return bookRepository.findAll()
            .map { it.toBook() }
    }

    fun remove(id: String) {
        if (!exists(id)) {
            throw BookNotFoundException(id)
        }
        log.info("Removing book $id")
        bookRepository.deleteById(id)
        eventPublisher.publishEvent(BookRemovedEvent(id))
    }

    fun exists(id: String): Boolean {
        return bookRepository.existsById(id)
    }

    fun findByExternalIds(externalIds: List<String>) =
        bookRepository.findByExternalIdIn(externalIds).map { it.toBook() }

    fun findByExternalId(externalId: String) = bookRepository.findByExternalId(externalId)?.toBook()

    fun findByIdsNoSecurity(ids: List<String>) = insecureBookRepository.findAllById(ids).map { it.toBook() }

    fun BookDocument.update(request: UpdateBookRequest): BookDocument {
        val newBoughtAt =
            if (request.status == BookStatus.BOUGHT && status != BookStatus.BOUGHT) clock.instant() else boughtAt
        val newReadAt = if (request.status == BookStatus.READ && status != BookStatus.READ) clock.instant() else readAt
        return copy(
            version = version,
            status = request.status,
            boughtAt = newBoughtAt,
            readAt = newReadAt,
            title = request.title,
            authors = request.authors,
            publicationDate = request.publicationDate,
            description = request.description,
            notes = request.notes,
            isbn13 = request.isbn13,
            isbn10 = request.isbn10,
            language = request.language,
            pageCount = request.pageCount,
            publisher = request.publisher
        )
    }

    fun yearsForReadAt(): List<Int> {
        return bookRepository.findYearsForReadAt()
    }

    fun findByReadBooksByYear(year: Int): List<Book> {
        val startOfYear = ZonedDateTime.of(year, 1, 1, 0, 0, 0, 0, ZoneId.of("Europe/Berlin"))
        val startOfFollowingYear = startOfYear.withYear(year + 1)
        return bookRepository.findByReadAtBetweenAndStatus(
            startOfYear.toInstant(),
            startOfFollowingYear.toInstant(),
            BookStatus.READ
        ).map { it.toBook() }
    }

}

fun BookDocument.toBook() =
    Book(
        id = id!!,
        externalId = externalId,
        createdAt = createdAt!!,
        modifiedAt = modifiedAt!!,
        version = version!!,
        status = status,
        boughtAt = boughtAt,
        readAt = readAt,
        title = title,
        authors = authors,
        publicationDate = publicationDate,
        description = description,
        notes = notes,
        thumbnailUrl = thumbnailUrl,
        googleUrl = googleUrl,
        isbn10 = isbn10,
        isbn13 = isbn13,
        language = language,
        pageCount = pageCount,
        publisher = publisher
    )

fun GoogleVolume.toBookDocument(): BookDocument {
    return BookDocument(
        externalId = id,
        title = volumeInfo.title!!,
        authors = volumeInfo.authors,
        description = volumeInfo.description,
        publicationDate = volumeInfo.publishedDate,
        isbn10 = volumeInfo.isbn10,
        isbn13 = volumeInfo.isbn13,
        thumbnailUrl = volumeInfo.thumbnailUrl,
        googleUrl = volumeInfo.infoLink,
        language = volumeInfo.language,
        pageCount = volumeInfo.pageCount,
        publisher = volumeInfo.publisher,
        status = BookStatus.WANTED
    )
}