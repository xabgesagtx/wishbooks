package com.canceledsystems.wishbooks.books.rest

import com.canceledsystems.wishbooks.books.common.BookStatus
import java.time.Instant

data class BookApiDTO(val id: String,
                      val externalId: String,
                      val createdAt: Instant,
                      val modifiedAt: Instant,
                      val version: Int,
                      val status: BookStatus,
                      val boughtAt: Instant? = null,
                      val readAt: Instant? = null,
                      val title: String,
                      val authors: List<String>,
                      val publicationDate: String? = null,
                      val description: String? = null,
                      val notes: String? = null,
                      val thumbnailUrl: String? = null,
                      val googleUrl: String,
                      val isbn10: String? = null,
                      val isbn13: String? = null,
                      val language: String,
                      val pageCount: Int? = null,
                      val publisher: String? = null)
