package com.canceledsystems.wishbooks.books.rest

import com.canceledsystems.wishbooks.books.common.BookStatus
import com.canceledsystems.wishbooks.books.service.Book
import com.canceledsystems.wishbooks.books.service.BookService
import com.canceledsystems.wishbooks.books.service.UpdateBookRequest
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("api/books")
class BooksController(private val bookService: BookService) {

    @GetMapping("")
    fun findAll(): List<BookApiDTO> {
        return bookService.findAll().map { it.toDTO() }
    }

    @GetMapping("{id}")
    fun get(@PathVariable("id") id: String): BookApiDTO {
        return bookService.get(id)
                ?.toDTO()
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find book with id $id")
    }

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: String, @RequestBody requestDTO: UpdateBookRequestDTO): BookApiDTO {
        return bookService.update(id, requestDTO.toRequest()).toDTO()
    }

    @PutMapping("{id}/status")
    fun updateStatus(@PathVariable("id") id: String, @RequestParam("status") status: BookStatus) {
        bookService.updateStatus(id, status)
    }

    @PostMapping("add")
    fun add(@RequestParam("externalId") externalId: String): BookApiDTO {
        return bookService.add(externalId).toDTO()
    }

    @DeleteMapping("{id}")
    fun remove(@PathVariable("id") id: String) {
        bookService.remove(id)
    }

    @GetMapping("years/read")
    fun years(): List<Int> {
        return bookService.yearsForReadAt()
    }

    @GetMapping("read/{year}")
    fun booksReadInYear(@PathVariable("year") year: Int): List<BookApiDTO> {
        return bookService.findByReadBooksByYear(year).map { it.toDTO() }
    }
}

fun Book.toDTO(): BookApiDTO =
        BookApiDTO(id = id,
                externalId = externalId,
                createdAt = createdAt,
                modifiedAt = modifiedAt,
                version = version,
                status = status,
                boughtAt = boughtAt,
                readAt = readAt,
                title = title,
                authors = authors,
                publicationDate = publicationDate,
                description = description,
                notes = notes,
                thumbnailUrl = thumbnailUrl,
                googleUrl = googleUrl,
                isbn13 = isbn13,
                isbn10 = isbn10,
                language = language,
                pageCount = pageCount,
                publisher = publisher)

fun UpdateBookRequestDTO.toRequest() =
        UpdateBookRequest(status = status,
                version = version,
                title = title,
                authors = authors,
                publicationDate = publicationDate,
                description = description,
                notes = notes,
                isbn10 = isbn10,
                isbn13 = isbn13,
                language = language,
                pageCount = pageCount,
                publisher = publisher)