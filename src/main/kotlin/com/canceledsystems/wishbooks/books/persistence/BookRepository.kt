package com.canceledsystems.wishbooks.books.persistence

import com.canceledsystems.wishbooks.books.common.BookStatus
import org.springframework.data.repository.CrudRepository
import java.time.Instant

interface BookRepository : CrudRepository<BookDocument, String>, CustomBookRepository {

    fun findAllByUser(user: String): List<BookDocument>

    fun findByIdAndUser(id: String, user: String): BookDocument?

    fun existsByIdAndUser(id: String, user: String): Boolean

    fun findByExternalIdInAndUser(externalIds: List<String>, user: String): List<BookDocument>

    fun findByExternalIdAndUser(externalId: String, user: String): BookDocument?

    fun findByReadAtBetweenAndStatusAndUserOrderByReadAtAsc(
        start: Instant, end: Instant, read: BookStatus, user: String
    ): List<BookDocument>

    fun deleteByIdAndUser(id: String, user: String)


}