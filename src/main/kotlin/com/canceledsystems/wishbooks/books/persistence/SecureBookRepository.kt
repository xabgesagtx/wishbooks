package com.canceledsystems.wishbooks.books.persistence

import com.canceledsystems.wishbooks.books.common.BookStatus
import com.canceledsystems.wishbooks.common.CurrentUserService
import org.springframework.stereotype.Repository
import java.time.Instant

@Repository
class SecureBookRepository(
    private val currentUserService: CurrentUserService,
    private val bookRepository: BookRepository
) {

    fun save(document: BookDocument): BookDocument = bookRepository.save(document)

    fun findAll(): List<BookDocument> = bookRepository.findAllByUser(currentUserService.currentUserOrFail())

    fun findById(id: String): BookDocument? =
        bookRepository.findByIdAndUser(id, currentUserService.currentUserOrFail())

    fun existsById(id: String): Boolean = bookRepository.existsByIdAndUser(id, currentUserService.currentUserOrFail())

    fun findByExternalIdIn(ids: List<String>): List<BookDocument> =
        bookRepository.findByExternalIdInAndUser(ids, currentUserService.currentUserOrFail())

    fun findByExternalId(externalId: String): BookDocument? =
        bookRepository.findByExternalIdAndUser(externalId, currentUserService.currentUserOrFail())

    fun findByReadAtBetweenAndStatus(start: Instant, end: Instant, status: BookStatus): List<BookDocument> =
        bookRepository.findByReadAtBetweenAndStatusAndUserOrderByReadAtAsc(start, end, status, currentUserService.currentUserOrFail())

    fun deleteById(id: String) = bookRepository.deleteByIdAndUser(id, currentUserService.currentUserOrFail())

    fun findYearsForReadAt(): List<Int> = bookRepository.findYearsForReadAt(currentUserService.currentUserOrFail())

}