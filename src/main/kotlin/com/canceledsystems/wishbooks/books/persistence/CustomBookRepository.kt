package com.canceledsystems.wishbooks.books.persistence

import com.canceledsystems.wishbooks.books.common.BookStatus
import org.bson.Document
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation.*
import org.springframework.data.mongodb.core.aggregation.DateOperators
import org.springframework.data.mongodb.core.aggregation.DateOperators.Year.yearOf
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.stereotype.Repository
import java.time.ZoneId

interface CustomBookRepository {
    fun findYearsForReadAt(user: String): List<Int>
}

@Repository
class CustomBookRepositoryImpl(private val mongoTemplate: MongoTemplate) : CustomBookRepository {
    override fun findYearsForReadAt(user: String): List<Int> {
        val match = match(Criteria.where("status").`is`(BookStatus.READ).and("user").`is`(user))
        val project =
            project("_id").and(yearOf("readAt").withTimezone(DateOperators.Timezone.fromZone(ZoneId.of("Europe/Berlin"))))
                .`as`("year")
        val group = group("year")
        val aggregation = newAggregation(BookDocument::class.java, match, project, group)
        return mongoTemplate.aggregate(aggregation, Document::class.java).mappedResults.map { it["_id"] as Int }.sorted()
    }

}