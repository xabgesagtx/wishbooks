package com.canceledsystems.wishbooks.books.persistence

import com.canceledsystems.wishbooks.books.common.BookStatus
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document("books")
data class BookDocument(val id: String? = null,
                        val externalId: String,
                        @CreatedBy val user: String? = null,
                        @CreatedDate val createdAt: Instant? = null,
                        @LastModifiedDate val modifiedAt: Instant? = null,
                        @Version val version: Int? = null,
                        val boughtAt: Instant? = null,
                        val readAt: Instant? = null,
                        val status: BookStatus,
                        val title: String,
                        val authors: List<String>,
                        val publicationDate: String? = null,
                        val notes: String? = null,
                        val description: String? = null,
                        val thumbnailUrl: String? = null,
                        val googleUrl: String,
                        val isbn10: String? = null,
                        val isbn13: String? = null,
                        val language: String,
                        val pageCount: Int? = null,
                        val publisher: String? = null)