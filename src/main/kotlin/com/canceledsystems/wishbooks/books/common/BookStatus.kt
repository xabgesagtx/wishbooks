package com.canceledsystems.wishbooks.books.common

enum class BookStatus {
    WANTED, BOUGHT, READ
}
