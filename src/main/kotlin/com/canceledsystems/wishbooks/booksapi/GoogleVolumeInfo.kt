package com.canceledsystems.wishbooks.booksapi

data class GoogleVolumeInfo(val title: String?,
                            val authors: List<String>,
                            val industryIdentifiers: List<GoogleIndustryIdentifiers> = emptyList(),
                            val imageLinks: GoogleImageLinks? = null,
                            val description: String? = null,
                            val publishedDate: String? = null,
                            val infoLink: String,
                            val language: String,
                            val pageCount: Int? = null,
                            val publisher: String? = null
) {
    val thumbnailUrl: String?
        get() {
            return imageLinks?.thumbnail ?: imageLinks?.smallThumbnail
        }

    val isbn10: String?
        get() {
            return industryIdentifiers.firstOrNull { it.type == "ISBN_10" }?.identifier
        }

    val isbn13: String?
        get() {
            return industryIdentifiers.firstOrNull { it.type == "ISBN_13" }?.identifier
        }
}