package com.canceledsystems.wishbooks.booksapi

data class GoogleVolumes(val items: List<GoogleVolume>,
                         val totalItems: Int)