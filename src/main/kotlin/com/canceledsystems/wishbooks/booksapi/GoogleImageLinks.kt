package com.canceledsystems.wishbooks.booksapi

data class GoogleImageLinks(val smallThumbnail: String?,
                            val thumbnail: String?)