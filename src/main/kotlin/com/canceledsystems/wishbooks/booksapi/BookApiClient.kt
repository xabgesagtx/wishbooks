package com.canceledsystems.wishbooks.booksapi

import com.google.api.client.http.HttpResponseException
import com.google.api.services.books.v1.Books
import com.google.api.services.books.v1.model.Volume
import com.google.api.services.books.v1.model.Volumes
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component

@Component
class BookApiClient(private val books: Books) {

    fun search(query: String, language: String?, page: Int, pageSize: Int): GoogleVolumes {
        val request = books.volumes().list(query)
        request.maxResults = pageSize.toLong()
        request.startIndex = page * pageSize.toLong()
        language?.let { request.langRestrict = it }
        return request.execute().toGoogleVolumes()
    }

    fun details(volumeId: String): GoogleVolume? {
        return try {
            books.volumes().get(volumeId).execute().toGoogleVolume()
        } catch (e: HttpResponseException) {
            if (e.statusCode == HttpStatus.NOT_FOUND.value()) {
                null
            } else {
                throw BooksApiException("Failed to retrieve book details for volumeId $volumeId", e)
            }
        }
    }

}

fun Volumes.toGoogleVolumes() = GoogleVolumes(
        totalItems = totalItems,
        items = items?.map { it.toGoogleVolume() } ?: emptyList()
)

fun Volume.toGoogleVolume() = GoogleVolume(
        id = id,
        volumeInfo = volumeInfo.toGoogleVolumeInfo()
)

fun Volume.VolumeInfo.toGoogleVolumeInfo() = GoogleVolumeInfo(
        title = title,
        authors = authors ?: emptyList(),
        industryIdentifiers = industryIdentifiers?.map { it.toGoogleIndustryIdentifiers()} ?: emptyList(),
        infoLink = infoLink,
        imageLinks = imageLinks?.toGoogleImageLinks(),
        description = description,
        publishedDate = publishedDate,
        pageCount = pageCount,
        publisher = publisher,
        language = language
)

private fun Volume.VolumeInfo.ImageLinks.toGoogleImageLinks() = GoogleImageLinks(
        thumbnail = thumbnail,
        smallThumbnail = smallThumbnail
)

private fun Volume.VolumeInfo.IndustryIdentifiers.toGoogleIndustryIdentifiers() = GoogleIndustryIdentifiers(
        type = type,
        identifier = identifier
)