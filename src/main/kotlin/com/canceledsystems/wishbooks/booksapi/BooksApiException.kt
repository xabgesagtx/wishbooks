package com.canceledsystems.wishbooks.booksapi

class BooksApiException(message: String, cause: Throwable): RuntimeException(message, cause) {
}