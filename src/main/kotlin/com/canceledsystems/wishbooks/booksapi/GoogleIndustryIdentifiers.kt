package com.canceledsystems.wishbooks.booksapi

data class GoogleIndustryIdentifiers(val identifier: String,
                                     val type: String)