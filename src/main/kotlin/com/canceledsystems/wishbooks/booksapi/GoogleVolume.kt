package com.canceledsystems.wishbooks.booksapi

data class GoogleVolume(val id: String,
                        val volumeInfo: GoogleVolumeInfo)