package com.canceledsystems.wishbooks.booksapi

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("booksapi")
class ApiProperties {

    lateinit var apiKey: String
    lateinit var applicationName: String

}