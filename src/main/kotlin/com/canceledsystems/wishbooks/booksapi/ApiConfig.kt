package com.canceledsystems.wishbooks.booksapi

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.books.v1.Books
import com.google.api.services.books.v1.BooksRequestInitializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ApiConfig {

    @Bean
    fun books(apiProperties: ApiProperties): Books {
        val jacksonFactory = GsonFactory.getDefaultInstance()
        val trustedTransport = GoogleNetHttpTransport.newTrustedTransport()
        return Books.Builder(trustedTransport, jacksonFactory, null)
                .setApplicationName(apiProperties.applicationName)
                .setBooksRequestInitializer(BooksRequestInitializer(apiProperties.apiKey))
                .build()
    }
}