package com.canceledsystems.wishbooks.search.service

data class BookDetails(val id: String,
                       val title: String,
                       val authors: List<String>,
                       val publicationDate: String? = null,
                       val description: String? = null,
                       val thumbnailUrl: String? = null,
                       val googleUrl: String,
                       val isbn10: String? = null,
                       val isbn13: String? = null,
                       val language: String,
                       val pageCount: Int? = null,
                       val publisher: String? = null,
                       val internalId: String? = null)