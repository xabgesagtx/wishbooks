package com.canceledsystems.wishbooks.search.service

data class SearchResponse(val page: Int,
                          val numberOfPages: Int,
                          val books: List<BookSearchItem>)