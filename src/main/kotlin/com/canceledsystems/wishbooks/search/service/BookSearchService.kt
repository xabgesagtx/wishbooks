package com.canceledsystems.wishbooks.search.service

import com.canceledsystems.wishbooks.books.service.BookService
import com.canceledsystems.wishbooks.booksapi.BookApiClient
import com.canceledsystems.wishbooks.booksapi.GoogleVolume
import org.springframework.stereotype.Service
import kotlin.math.ceil

@Service
class BookSearchService(private val apiClient: BookApiClient,
                        private val bookService: BookService) {

    companion object {
        const val PAGE_SIZE = 40
    }

    fun search(term: String, language: String?, page: Int): SearchResponse {
        val volumes = apiClient.search(term, language, page, PAGE_SIZE)
        val ids = volumes.items.map { it.id }
        val idToInternalId = bookService.findByExternalIds(ids).associateBy({ it.externalId }, { it.id })
        val books = volumes.items.filter { it.volumeInfo.title != null }.map{ it.toBookSearchItem(idToInternalId)}
        val numberOfPages = ceil(volumes.totalItems / PAGE_SIZE.toDouble()).toInt()
        return SearchResponse(page = page,
                numberOfPages = numberOfPages,
                books = books)
    }

    fun details(id: String): BookDetails? {
        val internalId = bookService.findByExternalId(id)?.id
        return apiClient.details(id)?.toBookDetails(internalId)
    }

}

fun GoogleVolume.toBookSearchItem(idToInternalId: Map<String, String>): BookSearchItem {
    return BookSearchItem(id = id,
            internalId = idToInternalId[id],
            title = volumeInfo.title!!,
            authors = volumeInfo.authors,
            publicationDate = volumeInfo.publishedDate,
            thumbnailUrl = volumeInfo.thumbnailUrl)
}

fun GoogleVolume.toBookDetails(internalId: String?): BookDetails {
    return BookDetails(
            id = id,
            internalId = internalId,
            title = volumeInfo.title!!,
            authors = volumeInfo.authors,
            description = volumeInfo.description,
            publicationDate = volumeInfo.publishedDate,
            isbn10 = volumeInfo.isbn10,
            isbn13 = volumeInfo.isbn13,
            thumbnailUrl = volumeInfo.thumbnailUrl,
            googleUrl = volumeInfo.infoLink,
            language = volumeInfo.language,
            pageCount = volumeInfo.pageCount,
            publisher = volumeInfo.publisher)
}