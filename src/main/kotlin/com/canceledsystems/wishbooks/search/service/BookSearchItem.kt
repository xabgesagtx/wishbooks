package com.canceledsystems.wishbooks.search.service

data class BookSearchItem(val id: String,
                          val internalId: String?,
                          val authors: List<String>,
                          val title: String,
                          val publicationDate: String?,
                          val thumbnailUrl: String?)