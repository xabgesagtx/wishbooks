package com.canceledsystems.wishbooks.search.rest

import com.canceledsystems.wishbooks.search.service.BookDetails
import com.canceledsystems.wishbooks.search.service.BookSearchItem
import com.canceledsystems.wishbooks.search.service.BookSearchService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("api")
class SearchController(private val bookSearchService: BookSearchService) {

    @GetMapping("search")
    fun search(@RequestParam("query") query: String, @RequestParam("language") language: String?, @RequestParam("page") page: Int): SearchResponseDTO {
        val response = bookSearchService.search(query, language, page)
        return SearchResponseDTO(page = response.page,
                numberOfPages = response.numberOfPages,
                books = response.books.map { it.toDTO() })
    }

    @GetMapping("search/{id}")
    fun details(@PathVariable("id") id: String): BookDetailsDTO {
        return bookSearchService.details(id)?.toDTO()
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "No book with $id found")
    }


}

fun BookSearchItem.toDTO() = BookSearchItemDTO(id = id,
        internalId = internalId,
        title = title,
        authors = authors,
        publicationDate = publicationDate,
        thumbnailUrl = thumbnailUrl)

fun BookDetails.toDTO() = BookDetailsDTO(
        id = id,
        internalId = internalId,
        authors = authors,
        title = title,
        description = description,
        language = language,
        publisher = publisher,
        publicationDate = publicationDate,
        pageCount = pageCount,
        isbn10 = isbn10,
        isbn13 = isbn13,
        thumbnailUrl = thumbnailUrl,
        googleUrl = googleUrl)