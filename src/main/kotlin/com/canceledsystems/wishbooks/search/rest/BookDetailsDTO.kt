package com.canceledsystems.wishbooks.search.rest

data class BookDetailsDTO(val id: String,
                          val internalId: String?,
                          val title: String,
                          val authors: List<String>,
                          val publicationDate: String?,
                          val description: String?,
                          val thumbnailUrl: String?,
                          val googleUrl: String,
                          val isbn10: String?,
                          val isbn13: String?,
                          val language: String,
                          val pageCount: Int?,
                          val publisher: String? = null)