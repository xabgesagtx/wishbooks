package com.canceledsystems.wishbooks.search.rest

data class BookSearchItemDTO(val id: String,
                             val internalId: String?,
                             val title: String,
                             val authors: List<String>,
                             val publicationDate: String?,
                             val thumbnailUrl: String?)