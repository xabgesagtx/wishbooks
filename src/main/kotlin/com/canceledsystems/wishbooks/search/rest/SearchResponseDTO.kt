package com.canceledsystems.wishbooks.search.rest

data class SearchResponseDTO(val page: Int,
                             val numberOfPages: Int,
                             val books: List<BookSearchItemDTO>)
