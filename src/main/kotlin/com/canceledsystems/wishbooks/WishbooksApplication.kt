package com.canceledsystems.wishbooks

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WishbooksApplication

fun main(args: Array<String>) {
	runApplication<WishbooksApplication>(*args)
}
