package com.canceledsystems.wishbooks.lists.persistence

import org.springframework.data.annotation.*
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document
data class BookListDocument(@Id val id: String? = null,
                            @CreatedBy val user: String? = null,
                            @CreatedDate val createdAt: Instant? = null,
                            @LastModifiedDate val modifiedAt: Instant? = null,
                            @Version val version: Int? = null,
                            val name: String,
                            val description: String,
                            val publicAccess: Boolean,
                            val books: List<String> = emptyList())