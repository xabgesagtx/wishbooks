package com.canceledsystems.wishbooks.lists.persistence

import com.canceledsystems.wishbooks.common.CurrentUserService
import org.springframework.stereotype.Repository

@Repository
class SecureBookListRepository(private val bookListRepository: BookListRepository, private val currentUserService: CurrentUserService) {

    fun findAll() = bookListRepository.findAllByUser(currentUserService.currentUserOrFail())

    fun findById(id: String) = bookListRepository.findByIdAndUser(id, currentUserService.currentUserOrFail())

    fun existsById(id: String) = bookListRepository.existsByIdAndUser(id, currentUserService.currentUserOrFail())

    fun findByBooksContains(id: String) = bookListRepository.findByBooksContainsAndUser(id, currentUserService.currentUserOrFail())

    fun saveAll(lists: List<BookListDocument>): MutableIterable<BookListDocument> = bookListRepository.saveAll(lists)

    fun save(list: BookListDocument): BookListDocument = bookListRepository.save(list)

    fun deleteById(id: String) = bookListRepository.deleteByIdAndUser(id, currentUserService.currentUserOrFail())
}