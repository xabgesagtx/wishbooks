package com.canceledsystems.wishbooks.lists.persistence

import org.springframework.data.repository.CrudRepository

interface BookListRepository : CrudRepository<BookListDocument, String> {

    fun findAllByUser(user: String): List<BookListDocument>

    fun findByIdAndUser(id: String, user: String): BookListDocument?

    fun existsByIdAndUser(id: String, user: String): Boolean

    fun findByIdAndPublicAccessIsTrue(id: String): BookListDocument?

    fun findByBooksContainsAndUser(id: String, user: String): List<BookListDocument>

    fun deleteByIdAndUser(id: String, user: String)

}