package com.canceledsystems.wishbooks.lists.service

data class BookListItem(val id: String,
                        val authors: List<String>,
                        val title: String,
                        val publicationDate: String?,
                        val thumbnailUrl: String?,
                        val googleUrl: String,
                        val isbn10: String?,
                        val isbn13: String?,
                        val publisher: String?,
                        val language: String)
