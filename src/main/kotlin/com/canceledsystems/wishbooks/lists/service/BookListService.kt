package com.canceledsystems.wishbooks.lists.service

import com.canceledsystems.wishbooks.books.service.Book
import com.canceledsystems.wishbooks.books.service.BookRemovedEvent
import com.canceledsystems.wishbooks.books.service.BookService
import com.canceledsystems.wishbooks.lists.persistence.BookListDocument
import com.canceledsystems.wishbooks.lists.persistence.BookListRepository
import com.canceledsystems.wishbooks.lists.persistence.SecureBookListRepository
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service

@Service
class BookListService(
    private val repository: SecureBookListRepository,
    private val insecureRepository: BookListRepository,
    private val bookService: BookService
) {

    fun findAll() = repository.findAll().map { it.toBookListSummary() }

    fun findById(id: String): BookList? = repository.findById(id)?.toBookList()

    fun create(request: CreateBookListRequest): BookList {
        val bookList = BookListDocument(
            name = request.name, description = request.description, publicAccess = request.publicAccess
        )
        return repository.save(bookList).toBookList()
    }

    //    @Transactional
    fun addBook(listId: String, bookId: String) {
        repository.findById(listId)
            ?.let { it.copy(books = it.books + bookId) }
            ?.let { repository.save(it) }
    }

    //    @Transactional
    fun removeBook(listId: String, bookId: String) {
        repository.findById(listId)
            ?.let { it.copy(books = it.books - bookId) }
            ?.let { repository.save(it) }
    }

    //    @Transactional
    fun update(id: String, request: UpdateBookListRequest): BookList? =
        repository.findById(id)
            ?.update(request)
            ?.let { repository.save(it) }
            ?.toBookList()

    fun remove(id: String) {
        if (repository.existsById(id)) {
            repository.deleteById(id)
        }
    }

    @EventListener
    fun onBookRemoved(event: BookRemovedEvent) {
        val updatedLists = repository.findByBooksContains(event.id).map { it.copy(books = it.books - event.id) }
        repository.saveAll(updatedLists)
    }

    fun findPublicBookList(id: String) = insecureRepository.findByIdAndPublicAccessIsTrue(id)?.toBookList()

    private fun BookListDocument.toBookList(): BookList {
        return BookList(
            id = id!!,
            createdAt = createdAt!!,
            modifiedAt = modifiedAt!!,
            version = version!!,
            name = name,
            description = description,
            books = findBookListItemsOrdered(books),
            publicAccess = publicAccess
        )
    }

    private fun findBookListItemsOrdered(ids: List<String>): List<BookListItem> {
        val books = bookService.findByIdsNoSecurity(ids).associateBy({ it.id }, { it.toBookListItem() })
        return ids.mapNotNull { books[it] }
    }

}

private fun BookListDocument.toBookListSummary() = BookListSummary(
    id = id!!,
    createdAt = createdAt!!,
    modifiedAt = modifiedAt!!,
    name = name,
    description = description,
    bookIds = books
)

private fun BookListDocument.update(request: UpdateBookListRequest) = copy(
    version = request.version,
    name = request.name,
    description = request.description,
    books = request.bookIds,
    publicAccess = request.publicAccess
)

private fun Book.toBookListItem() = BookListItem(
    id = id,
    authors = authors,
    title = title,
    publicationDate = publicationDate,
    isbn10 = isbn10,
    isbn13 = isbn13,
    publisher = publisher,
    thumbnailUrl = thumbnailUrl,
    googleUrl = googleUrl,
    language = language
)


