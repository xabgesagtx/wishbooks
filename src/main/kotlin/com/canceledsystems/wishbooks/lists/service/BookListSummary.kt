package com.canceledsystems.wishbooks.lists.service

import java.time.Instant

data class BookListSummary(val id: String,
                           val createdAt: Instant,
                           val modifiedAt: Instant,
                           val name: String,
                           val description: String,
                           val bookIds: List<String>)