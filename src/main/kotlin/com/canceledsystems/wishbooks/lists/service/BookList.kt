package com.canceledsystems.wishbooks.lists.service

import java.time.Instant

data class BookList(val id: String,
                    val createdAt: Instant,
                    val modifiedAt: Instant,
                    val version: Int,
                    val name: String,
                    val description: String,
                    val publicAccess: Boolean,
                    val books: List<BookListItem>)