package com.canceledsystems.wishbooks.lists.service

data class CreateBookListRequest(val name: String,
                                 val description: String,
                                 val publicAccess: Boolean)