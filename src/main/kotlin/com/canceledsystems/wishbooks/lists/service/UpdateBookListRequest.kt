package com.canceledsystems.wishbooks.lists.service

data class UpdateBookListRequest(val version: Int,
                                 val name: String,
                                 val description: String,
                                 val bookIds: List<String>,
                                 val publicAccess: Boolean)
