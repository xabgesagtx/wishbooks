package com.canceledsystems.wishbooks.lists.rest

import com.canceledsystems.wishbooks.lists.service.*
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("api/lists")
class BookListController(private val bookListService: BookListService) {

    @GetMapping
    fun findAll() = bookListService.findAll().map { it.toDTO() }

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: String) =
            bookListService.findById(id)
                    ?.toDTO()

    @PostMapping
    fun create(@RequestBody request: CreateBookListRequestDTO) =
            bookListService.create(request.toRequest()).toDTO()

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: String, @RequestBody request: UpdateBookListRequestDTO) =
            bookListService.update(id, request.toRequest())?.toDTO() ?: ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find book $id")

    @DeleteMapping("{id}")
    fun remove(@PathVariable("id") id: String) {
        bookListService.remove(id)
    }

    @PostMapping("{id}/add")
    fun addBook(@PathVariable("id") listId: String, @RequestParam("bookId") bookId: String) {
        bookListService.addBook(listId, bookId)
    }

    @PostMapping("{id}/remove")
    fun removeBook(@PathVariable("id") listId: String, @RequestParam("bookId") bookId: String) {
        bookListService.removeBook(listId, bookId)
    }

}

private fun CreateBookListRequestDTO.toRequest() = CreateBookListRequest(name = name,
        description = description,
        publicAccess = publicAccess)

private fun UpdateBookListRequestDTO.toRequest() = UpdateBookListRequest(version = version,
        name = name,
        description = description,
        bookIds = bookIds,
        publicAccess = publicAccess)

private fun BookList.toDTO() = BookListDTO(id = id,
        createdAt = createdAt,
        modifiedAt = modifiedAt,
        version = version,
        name = name,
        description = description,
        publicAccess = publicAccess,
        books = books.map { it.toDTO() })

private fun BookListItem.toDTO() = BookListItemDTO(id = id,
        authors = authors,
        title = title,
        publicationDate = publicationDate,
        thumbnailUrl = thumbnailUrl,
        googleUrl = googleUrl,
        isbn10 = isbn10,
        isbn13 = isbn13,
        publisher = publisher,
        language = language)

private fun BookListSummary.toDTO() = BookListSummaryDTO(id = id,
        createdAt = createdAt,
        modifiedAt = modifiedAt,
        name = name,
        description = description,
        bookIds = bookIds)
