package com.canceledsystems.wishbooks.lists.rest

data class CreateBookListRequestDTO(val name: String,
                                    val description: String,
                                    val publicAccess: Boolean)