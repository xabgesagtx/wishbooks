package com.canceledsystems.wishbooks.lists.rest

import java.time.Instant

data class BookListDTO(val id: String,
                       val createdAt: Instant,
                       val modifiedAt: Instant,
                       val version: Int,
                       val name: String,
                       val description: String,
                       val publicAccess: Boolean,
                       val books: List<BookListItemDTO>)