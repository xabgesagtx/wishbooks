package com.canceledsystems.wishbooks.lists.rest

data class PublicBookListDTO(val name: String,
                             val description: String,
                             val books: List<PublicBookListItemDTO>)