package com.canceledsystems.wishbooks.lists.rest

data class UpdateBookListRequestDTO(val version: Int,
                                    val name: String,
                                    val description: String,
                                    val bookIds: List<String>,
                                    val publicAccess: Boolean)