package com.canceledsystems.wishbooks.lists.rest

import java.time.Instant

data class BookListSummaryDTO(val id: String,
                              val createdAt: Instant,
                              val modifiedAt: Instant,
                              val name: String,
                              val description: String,
                              val bookIds: List<String>)