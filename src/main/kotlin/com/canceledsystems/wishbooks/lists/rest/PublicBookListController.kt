package com.canceledsystems.wishbooks.lists.rest

import com.canceledsystems.wishbooks.lists.service.BookList
import com.canceledsystems.wishbooks.lists.service.BookListItem
import com.canceledsystems.wishbooks.lists.service.BookListService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("public/lists")
class PublicBookListController(private val bookListService: BookListService) {

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: String) = bookListService.findPublicBookList(id)?.toDTO()
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find book list with $id")
}

private fun BookList.toDTO() = PublicBookListDTO(name = name,
        description = description,
        books = books.map { it.toDTO() })

private fun BookListItem.toDTO() = PublicBookListItemDTO(authors = authors,
        title = title,
        publicationDate = publicationDate,
        thumbnailUrl = thumbnailUrl,
        googleUrl = googleUrl,
        isbn10 = isbn10,
        isbn13 = isbn13,
        publisher = publisher,
        language = language)