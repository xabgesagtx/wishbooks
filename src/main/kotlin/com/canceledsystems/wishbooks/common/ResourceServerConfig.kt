package com.canceledsystems.wishbooks.common

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.web.SecurityFilterChain

@Configuration
class ResourceServerConfig() {

    @Bean
    fun configure(http: HttpSecurity): SecurityFilterChain {
        http.authorizeHttpRequests {
            it.requestMatchers("/api/**")
                .hasAuthority("SCOPE_wishbooks")
                .anyRequest()
                .permitAll()
        }
        http.oauth2ResourceServer {
            it.jwt(Customizer.withDefaults())
        }
        return http.build()
    }
}