package com.canceledsystems.wishbooks.common

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.domain.AuditorAware
import org.springframework.data.mongodb.config.EnableMongoAuditing


@Configuration
@EnableMongoAuditing(auditorAwareRef = "auditorProvider")
class BooksConfig {

    @Bean
    fun auditorProvider(currentUserService: CurrentUserService): AuditorAware<String> {
        return AuditorAware {
            currentUserService.currentUser()
        }
    }
}