package com.canceledsystems.wishbooks.common

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping


@Controller
class ViewController {

    @RequestMapping("/search/**","/not-found", "/some-error", "/books/**", "/lists/**", "/review/**")
    fun index(): String {
        return "forward:/index.html"
    }
}