package com.canceledsystems.wishbooks.common

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import org.springframework.stereotype.Service
import java.util.*

@Service
class CurrentUserService {

    fun currentUser(): Optional<String> {
        val authentication = SecurityContextHolder.getContext().authentication
        return Optional.ofNullable(authentication)
            .filter { it is JwtAuthenticationToken }
            .map {
                val jwt = it as JwtAuthenticationToken
                jwt.token.claims[USER_CLAIM].toString()
            }
    }

    fun currentUserOrFail(): String {
        val authentication = SecurityContextHolder.getContext().authentication ?: throw IllegalStateException("Authentication not set")
        if (authentication !is JwtAuthenticationToken) {
            throw IllegalStateException("Authentication is not of type ${JwtAuthenticationToken::class.java}")
        }
        return authentication.token.claims[USER_CLAIM]?.toString() ?: throw IllegalStateException("User claim not available")
    }

    companion object {
        const val USER_CLAIM = "sub"
    }
}