package com.canceledsystems.wishbooks

import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import

@SpringBootTest(properties = ["booksapi.api-key=TEST", "management.server.port=0"]) // workaround for https://github.com/spring-projects/spring-boot/issues/38554
@AutoConfigureMockMvc
@Import(MongoTestConfiguration::class)
class WishbooksApplicationTests {

    @Test
    fun contextLoads() {
    }

}
