package com.canceledsystems.wishbooks

import com.canceledsystems.wishbooks.books.rest.BookApiDTO
import com.canceledsystems.wishbooks.booksapi.BookApiClient
import com.canceledsystems.wishbooks.booksapi.GoogleVolume
import com.canceledsystems.wishbooks.booksapi.GoogleVolumeInfo
import com.canceledsystems.wishbooks.lists.rest.BookListDTO
import com.canceledsystems.wishbooks.lists.rest.CreateBookListRequestDTO
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

@SpringBootTest(properties = ["booksapi.api-key=TEST", "management.server.port=0"]) // workaround for https://github.com/spring-projects/spring-boot/issues/38554
@AutoConfigureMockMvc
@Import(MongoTestConfiguration::class)
class RemoveBookIT {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var objectMapper: ObjectMapper

    @MockBean
    lateinit var bookApiClient: BookApiClient


    private val externalId = "1"

    @Test
    fun `removing book removes it from book lists`() {
        val jwt = jwt().authorities(SimpleGrantedAuthority("SCOPE_wishbooks"))

        val book = addBook(jwt)

        val bookList = createList(jwt)

        addBookToList(book, bookList, jwt)

        assertBookIsInList(jwt)

        removeBookFromUserLibrary(book, jwt)

        assertBookWasRemovedFromList(jwt)
    }

    private fun assertBookWasRemovedFromList(jwt: SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor) {
        mockMvc.get("/api/lists") {
            with(jwt)
        }.andExpect {
            status { isOk() }
            jsonPath("[0].bookIds") { isEmpty() }
        }
    }

    private fun removeBookFromUserLibrary(
        book: BookApiDTO,
        jwt: SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor
    ) {
        mockMvc.delete("/api/books/{id}", book.id) {
            with(jwt)
        }.andExpect { status { isOk() } }
    }

    private fun assertBookIsInList(jwt: SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor) {
        mockMvc.get("/api/lists") {
            with(jwt)
        }.andExpect {
            status { isOk() }
            jsonPath("[0].bookIds", hasSize<Any>(1))
        }
    }

    private fun addBookToList(
        book: BookApiDTO,
        bookList: BookListDTO,
        jwt: SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor
    ) {
        mockMvc.post("/api/lists/{id}/add", bookList.id) {
            param("bookId", book.id)
            with(csrf())
            with(jwt)
        }.andExpect { status { isOk() } }
    }

    private fun createList(jwt: SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor): BookListDTO {
        val createBookListRequest = CreateBookListRequestDTO("name", "description", false)
        val createBookListResponse = mockMvc.post("/api/lists") {
            content = objectMapper.writeValueAsString(createBookListRequest)
            contentType = MediaType.APPLICATION_JSON
            with(csrf())
            with(jwt)
        }.andExpect { status { isOk() } }.andReturn()
        return objectMapper.readValue(createBookListResponse.response.contentAsString, BookListDTO::class.java)
    }

    private fun addBook(jwt: SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor): BookApiDTO {
        val googleVolume = GoogleVolume(
            id = externalId,
            volumeInfo = GoogleVolumeInfo(
                title = "title",
                language = "en",
                authors = emptyList(),
                infoLink = "https://example.com"
            )
        )
        whenever(bookApiClient.details(externalId)).thenReturn(googleVolume)


        val addBookResponse = mockMvc.post("/api/books/add") {
            param("externalId", externalId)
            with(csrf())
            with(jwt)
        }.andExpect { status { isOk() } }.andReturn()
        return objectMapper.readValue(addBookResponse.response.contentAsString, BookApiDTO::class.java)
    }

}