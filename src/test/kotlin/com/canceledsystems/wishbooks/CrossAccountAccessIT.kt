package com.canceledsystems.wishbooks

import com.canceledsystems.wishbooks.books.rest.BookApiDTO
import com.canceledsystems.wishbooks.booksapi.BookApiClient
import com.canceledsystems.wishbooks.booksapi.GoogleVolume
import com.canceledsystems.wishbooks.booksapi.GoogleVolumeInfo
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.Test
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Import
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post


@SpringBootTest(properties = ["booksapi.api-key=TEST", "management.server.port=0"]) // workaround for https://github.com/spring-projects/spring-boot/issues/38554
@AutoConfigureMockMvc
@Import(MongoTestConfiguration::class)
class CrossAccountAccessIT(
    @param:Autowired private val mockMvc: MockMvc,
    @param:Autowired private val objectMapper: ObjectMapper
) {

    @MockBean
    lateinit var bookApiClient: BookApiClient

    @Test
    fun `user can only access books from the same account`() {
        val jwtUser1 = jwt().jwt {
            it.subject("user1")
        }.authorities(SimpleGrantedAuthority("SCOPE_wishbooks"))
        val jwtUser2 = jwt().jwt {
            it.subject("user2")
        }.authorities(SimpleGrantedAuthority("SCOPE_wishbooks"))

        val book = addBook(jwtUser1)

        mockMvc.get("/api/books/{id}", book.id) {
            with(jwtUser1)
        }.andExpect {
            status {
                isOk()
            }
            content {
                jsonPath("$.id", `is`(book.id))
                jsonPath("$.title", `is`(book.title))
            }
        }

        mockMvc.get("/api/books/{id}", book.id) {
            with(jwtUser2)
        }.andExpect {
            status {
                isNotFound()
            }
        }

        mockMvc.delete("/api/books/{id}", book.id) {
            with(jwtUser2)
        }.andExpect {
            status {
                isNotFound()
            }
        }

        mockMvc.delete("/api/books/{id}", book.id) {
            with(jwtUser1)
        }.andExpect {
            status {
                isOk()
            }
        }

    }

    private fun addBook(jwt: JwtRequestPostProcessor): BookApiDTO {
        val googleVolume = GoogleVolume(
            id = EXTERNAL_ID,
            volumeInfo = GoogleVolumeInfo(
                title = "title",
                language = "en",
                authors = emptyList(),
                infoLink = "https://example.com"
            )
        )
        whenever(bookApiClient.details(EXTERNAL_ID)).thenReturn(googleVolume)

        val addBookResponse = mockMvc.post("/api/books/add") {
            param("externalId", EXTERNAL_ID)
            with(csrf())
            with(jwt)
        }.andExpect { status { isOk() } }.andReturn()
        return objectMapper.readValue(addBookResponse.response.contentAsString, BookApiDTO::class.java)
    }

    companion object {

        const val EXTERNAL_ID = "externalId"

    }
}