package com.canceledsystems.wishbooks.books.service

import com.canceledsystems.wishbooks.books.common.BookStatus
import com.canceledsystems.wishbooks.books.persistence.BookDocument
import com.canceledsystems.wishbooks.books.persistence.BookRepository
import com.canceledsystems.wishbooks.books.persistence.SecureBookRepository
import com.canceledsystems.wishbooks.booksapi.BookApiClient
import com.canceledsystems.wishbooks.booksapi.GoogleVolume
import com.canceledsystems.wishbooks.booksapi.GoogleVolumeInfo
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import org.springframework.context.ApplicationEventPublisher
import java.time.Clock
import java.time.Instant
import java.time.ZoneOffset

@ExtendWith(MockitoExtension::class)
internal class BookServiceTest(
    @param:Mock private val apiClient: BookApiClient,
    @param:Mock private val repo: SecureBookRepository,
    @param:Mock private val insecureRepository: BookRepository,
    @param:Mock private val eventPublisher: ApplicationEventPublisher,
) {

    private val bookService =
        BookService(repo, insecureRepository, apiClient, eventPublisher, Clock.fixed(NEW_STATUS_TIME, ZoneOffset.UTC))

    @Test
    fun `add fails because book doesn't exist in api`() {
        whenever(apiClient.details(EXTERNAL_ID)).thenReturn(null)
        assertThrows<ExternalBookNotFoundException> { bookService.add(EXTERNAL_ID) }
    }

    @Test
    fun `add succeeds`() {
        val googleVolume = GoogleVolume(
            id = EXTERNAL_ID, volumeInfo = GoogleVolumeInfo(
                title = TITLE,
                authors = AUTHORS,
                infoLink = GOOGLE_URL,
                language = LANGUAGE
            )
        )
        whenever(apiClient.details(EXTERNAL_ID)).thenReturn(googleVolume)

        val bookDocument = BookDocument(
            externalId = EXTERNAL_ID,
            title = TITLE,
            authors = AUTHORS,
            googleUrl = GOOGLE_URL,
            language = LANGUAGE,
            status = BookStatus.WANTED
        )
        val savedBookDocument = bookDocument.copy(
            id = ID,
            version = VERSION,
            createdAt = CREATED_AT,
            modifiedAt = MODIFIED_AT
        )
        whenever(repo.save(bookDocument)).thenReturn(savedBookDocument)

        val book = Book(
            id = ID,
            externalId = EXTERNAL_ID,
            title = TITLE,
            authors = AUTHORS,
            googleUrl = GOOGLE_URL,
            language = LANGUAGE,
            status = BookStatus.WANTED,
            createdAt = CREATED_AT,
            modifiedAt = MODIFIED_AT,
            version = VERSION
        )
        assertThat(bookService.add(EXTERNAL_ID))
            .isEqualTo(book)
    }

    @Test
    fun `update fails when book doesn't exist`() {
        val newTitle = "new title"
        val newAuthors = listOf("new author")
        val newLanguage = "en"
        val updateBookRequest = UpdateBookRequest(
            status = BookStatus.BOUGHT,
            version = VERSION,
            title = newTitle,
            authors = newAuthors,
            language = newLanguage
        )
        whenever(repo.findById(ID)).thenReturn(null)
        assertThrows<BookNotFoundException> { bookService.update(ID, updateBookRequest) }
    }

    @Test
    fun `update returns updated book on success`() {
        val newTitle = "new title"
        val newAuthors = listOf("new author")
        val newLanguage = "en"
        val updateBookRequest = UpdateBookRequest(
            status = BookStatus.BOUGHT,
            version = VERSION,
            title = newTitle,
            authors = newAuthors,
            language = newLanguage
        )
        val storedBookDocument = BookDocument(
            id = ID,
            externalId = EXTERNAL_ID,
            title = TITLE,
            authors = AUTHORS,
            googleUrl = GOOGLE_URL,
            language = LANGUAGE,
            status = BookStatus.WANTED,
            version = VERSION,
            createdAt = CREATED_AT,
            modifiedAt = MODIFIED_AT
        )
        whenever(repo.findById(ID)).thenReturn(storedBookDocument)

        val updatedBookDocument = storedBookDocument.copy(
            title = newTitle,
            authors = newAuthors,
            status = BookStatus.BOUGHT,
            boughtAt = NEW_STATUS_TIME,
            language = newLanguage
        )
        val newModifiedAt = Instant.EPOCH.plusSeconds(2)
        val newVersion = VERSION + 1
        val updatedBookDocumentAfterSaving = updatedBookDocument.copy(
            modifiedAt = newModifiedAt,
            version = newVersion
        )
        whenever(repo.save(updatedBookDocument)).thenReturn(updatedBookDocumentAfterSaving)

        val book = Book(
            id = ID,
            externalId = EXTERNAL_ID,
            title = newTitle,
            authors = newAuthors,
            googleUrl = GOOGLE_URL,
            language = newLanguage,
            status = BookStatus.BOUGHT,
            boughtAt = NEW_STATUS_TIME,
            createdAt = CREATED_AT,
            modifiedAt = newModifiedAt,
            version = newVersion
        )
        assertThat(bookService.update(ID, updateBookRequest))
            .isEqualTo(book)
    }

    @Test
    fun `remove fails when book doesn't exist`() {
        whenever(repo.existsById(ID)).thenReturn(false)
        assertThrows<BookNotFoundException> { bookService.remove(ID) }
    }

    @Test
    fun `remove emits event on success`() {
        whenever(repo.existsById(ID)).thenReturn(true)
        bookService.remove(ID)
        verify(repo).deleteById(ID)
        verify(eventPublisher).publishEvent(BookRemovedEvent(ID))
    }

    @Test
    fun `updateStatus fails when book doesn't exist`() {
        whenever(repo.findById(ID)).thenReturn(null)
        assertThrows<BookNotFoundException> { bookService.updateStatus(ID, BookStatus.READ) }
    }

    @Test
    fun `updateStatus succeeds when book exists`() {
        val storedBookDocument = BookDocument(
            id = ID,
            externalId = EXTERNAL_ID,
            title = TITLE,
            authors = AUTHORS,
            googleUrl = GOOGLE_URL,
            language = LANGUAGE,
            status = BookStatus.WANTED,
            version = VERSION,
            createdAt = CREATED_AT,
            modifiedAt = MODIFIED_AT
        )
        whenever(repo.findById(ID)).thenReturn(storedBookDocument)
        bookService.updateStatus(ID, BookStatus.READ)

        verify(repo).findById(ID)
        val updatedBookDocument = storedBookDocument.copy(status = BookStatus.READ, readAt = NEW_STATUS_TIME)
        verify(repo).save(updatedBookDocument)
    }


    companion object {
        const val ID = "id"
        const val EXTERNAL_ID = "externalId"
        const val TITLE = "title"
        val AUTHORS = listOf("author")
        const val GOOGLE_URL = "https://exmaple.com"
        const val LANGUAGE = "de"
        val CREATED_AT: Instant = Instant.EPOCH
        val MODIFIED_AT: Instant = Instant.EPOCH.plusSeconds(1)
        val NEW_STATUS_TIME: Instant = Instant.EPOCH.plusSeconds(2)
        const val VERSION = 0
    }
}