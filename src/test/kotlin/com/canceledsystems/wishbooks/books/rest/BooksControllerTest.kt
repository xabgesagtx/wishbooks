package com.canceledsystems.wishbooks.books.rest

import com.canceledsystems.wishbooks.books.common.BookStatus
import com.canceledsystems.wishbooks.books.service.BookNotFoundException
import com.canceledsystems.wishbooks.books.service.BookService
import org.junit.jupiter.api.Test
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.put

@WebMvcTest(BooksController::class)
internal class BooksControllerTest(
    @param:Autowired private val mockMvc: MockMvc
) {

    @MockBean
    private lateinit var bookService: BookService

    @Test
    fun `updateStatus returns 404 when book cannot be found`() {
        whenever(bookService.updateStatus(ID, BookStatus.BOUGHT)).thenThrow(BookNotFoundException::class.java)
        mockMvc.put("/api/books/{id}/status", ID) {
            with(createJwt())
            param("status", "BOUGHT")
        }.andExpect {
            status {
                isNotFound()
            }
        }

        verify(bookService).updateStatus(ID, BookStatus.BOUGHT)
    }

    @Test
    fun `updateStatus returns 200 on success`() {
        mockMvc.put("/api/books/{id}/status", ID) {
            param("status", "BOUGHT")
            with(createJwt())
        }.andExpect {
            status {
                isOk()
            }
        }


    }

    private fun createJwt() = jwt().authorities(SimpleGrantedAuthority("SCOPE_wishbooks"))

    companion object {
        const val ID = "id"
    }
}