# Wishbooks


[![pipeline status](https://gitlab.com/xabgesagtx/wishbooks/badges/master/pipeline.svg)](https://gitlab.com/xabgesagtx/wishbooks/commits/master)

![Wishbooks](screenshots/screenshot.png "Wishbooks screenshot")

A website for keeping track of the books you want to buy.

Features:
* search books via [Google Books API](https://developers.google.com/books)
* save them to your own library
* create list of books (public or private)
* edit book details in your library
* mark books as wanted, bought or read
* OAuth2 based authentication

Technologies used:
* backend
    * spring boot based application
    * written in kotlin
    * mongodb for storing books
    * using spring security for authentication
    * built with gradle
* frontend
    * written in typescript
    * using angular as main framework
    * bootstrap for layout
    * built with NPM

## Dependencies

* java 17 for building backend, node 10+ for frontend
* docker or java 17 for running
* OAuth2 identity provider for authentication

## Build

Wishbooks comes with a gradle wrapper to build the application as jar

```bash
./gradlew build
```

Gradle is configured to not only build the backend code but also build the typescript frontend code and package them together in a single jar file.

## Configuration

As wishbooks depends on external systems, there is some configuration needed.

The easiest way for a spring boot application is to create an application.yml

```yaml
# Google Book API configuration
booksapi:
    apiKey: YOUR_GOOGLE_BOOKS_API_KEY
# MongoDB configuration
spring:
  data:
    mongodb:
      database: DB_NAME
      host: HOSTNAME
      username: USERNAME
      password: PASSWORD
# OAuth2 configuration
security:
    oauth2:
      resourceserver:
        jwt:
          jwk-set-uri: PATH_TO_THE_CERTIFICATES_OF_OAUTH2_PROVIDER
```

__NOTE__: The oauth2 provider and client id is currently hard coded in angular and needs to be changed if you want to use a different provider.

## Development

To run in development you have to provide at least the Google API key.

For convenience there is a `docker-compose.yml` to start a local mongo instance. This means you can leave out the mongo configuraiton.

Run the following commands to prepare local development:
```bash
docker-compose up -d
cd frontend
npm install
npm run start
```

Afterwards start the java backend in your favorite IDE providing the necessary configuration outlined above.

## Run in production

### Jar

You can build the application yourself and run the jar:

```bash
java -jar build/libs/wishbooks.jar
```

Make sure you put the application.yml with the required configurations in the folder where you run this command.
